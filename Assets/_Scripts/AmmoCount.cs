﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoCount : MonoBehaviour {

    private Text uiText;
    protected vp_FPPlayerEventHandler m_PlayerEventHandler = null;


    // Use this for initialization
    void Awake () {
        uiText = GetComponent<Text>();
        vp_PlayerEventHandler
        m_PlayerEventHandler = transform.root.GetComponentInChildren<vp_FPPlayerEventHandler>();

    }
	
	// Update is called once per frame
	void Update () {

        uiText.text = (vp_LocalPlayer.CurrentAmmo + "/" +
(vp_LocalPlayer.CurrentMaxAmmo)).ToString();

    }
}
