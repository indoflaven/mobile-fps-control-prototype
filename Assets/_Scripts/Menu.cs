﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using MSP_Input;
using ControlFreak2;

public class Menu : MonoBehaviour
{

    public GameObject ControlLayer1;

    public vp_UICrosshair aimAssistSettings;
    public GameObject aimAssistToggle;
    public GameObject aimAssistStrengthInputField;
    public GameObject aimAssistStartRadiusInputField;
    public GameObject aimAssistContinueRadiusInputField;
    public GameObject aimAssistMaxDistInputField;
    public GameObject aimAssistHammerOnToggle;

    public GameObject autoFireToggle;
    public GameObject autoFireAcquisitionTimeInputField;
    public GameObject autoFireStartRadiusInputField;
    public GameObject autoFireContinueRadiusInputField;
    public GameObject autoFireMaxDistInputField;


    public UFPSControlFreak2Input aimSettings;
    public GameObject aimSensitivityHorInputField;
    public GameObject aimSensitivityVertInputField;
    public GameObject aimSensitivityHorZoomInputField;
    public GameObject aimSensitivityVertZoomInputField;
    public GameObject aimAccelerationToggle;
    public GameObject aimAccelerationThresholdInputField;
    public GameObject aimSmoothingStepsSlider;
    public GameObject aimSmoothingWeightSlider;
    public GameObject aimSmoothingOnlyOnReleaseToggle;

    //private GameObject gyroSettingsGameObject;
    public GyroAccel gyroSettings;
    public vp_FPCamera FPCamSettings;
    public GameObject gyroAimToggle;
    public GameObject gyroSensitivityHorInputField;
    public GameObject gyroSensitivityVertInputField;
    public GameObject gyroSensitivityHorZoomInputField;
    public GameObject gyroSensitivityVertZoomInputField;
    public GameObject gyroSmoothingSlider;

    public FlickDetectorFromCenter flickSettings;
    //public GameObject flickToggle;
    public GameObject flickDropDown;
    public GameObject flickSpeedInputField;
    public GameObject flickDistInputField;
    public GameObject flickTimeWindowInputField;
    public GameObject flickReqReleaseToggle;
    public GameObject flickStartFromCenterToggle;

    public SuperTouchZone rightSettings;
    public DoubleTapAndHold rightDoubleTapSettings;
    public GameObject rightTapDropDown;
    public GameObject rightDoubleTapDropDown;

    public PressureLevelBinding rightPressureSettings;
    public GameObject right3dTouchDropDown;
    public GameObject right3dTouchPressureSlider;

    public SuperTouchZone leftSettings;
    public DoubleTapAndHold leftDoubleTapSettings;
    public GameObject leftTapDropDown;
    public GameObject leftDoubleTapDropDown;

    public PressureLevelBinding leftPressureSettings;
    public GameObject left3dTouchDropDown;
    public GameObject left3dTouchPressureSlider;

    public HammerOn leftHammerOnSettings;
    public GameObject leftHammerOnDropDown;
    public GameObject leftHammerOnMaxGapInputField;
    public GameObject leftHammerOnMaxDistInputField;

    public HammerOn rightHammerOnSettings;
    public GameObject rightHammerOnDropDown;
    public GameObject rightHammerOnMaxGapInputField;
    public GameObject rightHammerOnMaxDistInputField;

    public SuperTouchZone bottomRightSettings;
    public GameObject bottomRightSwipeUpDropDown;

    public AccelerationTriggerFilter thrustSettings;
    public GameObject thrustDropDown;
    public GameObject thrustThresholdInputField;

    public TouchJoystick joystickSettings;
    public DynamicRegion joystickDynamicRegion;
    public GameObject dynamicJoystickToggle;
    public GameObject stickyJoystickToggle;
    public GameObject minAnalogRangeSlider;
    public GameObject maxAnalogRangeSlider;
    public GameObject startValueAnalogRangeInputField;

    public GameObject baseObj;
    public GameObject showBaseToggle;
    public GameObject hatObj;
    public GameObject showHatToggle;
    public GameObject directionObj;
    public GameObject showDirectionToggle;

    public GameObject stickyButton;
    public GameObject stickyButtonToggle;

    public List<string> axisList;
    private List<string> dirtySettings;

    //Buttons
    private List<String> profileList;
    private List<string> positionList;
    Dictionary<string, CoOrds> buttonPositions;

    public GameObject[] buttons;
    public GameObject[] buttonDropDowns;

    public GameObject profileDropDown;

    //private ES3File selectedProfileFile;
    public ES3File controlFile;
    private ES3Settings es3Settings;

    private bool initBool = false; //track initialization

    public struct CoOrds
    {
        public float x, y;

        public CoOrds(float p1, float p2)
        {
            x = p1;
            y = p2;
        }
    }



    private void Start()
    {

        dirtySettings = new List<String>();
        profileList = new List<String> { "Default.bytes", "3dTouch.bytes", "3dTouchDualTriggers.bytes", "3dTouchGyro.bytes", "DoubleTap.bytes", "HammerOn.bytes", "Custom" };
        var profileDescriptionList = new List<String> { "Default", "3dTouch fire and jump", "3d touch primary and secondary fire", "Auto-fire, 3d Touch seconardy fire and jump, Gyro", "Tap fire, Double-tap continuous fire", "Hammer-on fire and jump", "Custom" };

        profileDropDown.GetComponent<Dropdown>().AddOptions(profileDescriptionList);

        es3Settings = new ES3Settings();
        

        //selectedProfileFile = new ES3File("SelectedProfile.bytes");

        if (!ES3.KeyExists("profileSave"))
        {
            Debug.Log("profileSave does not exist");
            ES3.Save<int>("profileSave", 0);
            profileDropDown.GetComponent<Dropdown>().value = 0;
        }

        
        if(profileList[ES3.Load<int>("profileSave")] != "Custom")
        {
            es3Settings.location = ES3.Location.Resources;
            controlFile = new ES3File(profileList[ES3.Load<int>("profileSave")], es3Settings);
        }
        else
        {
            es3Settings.location = ES3.Location.File;
            controlFile = new ES3File("SaveData.es3", es3Settings);
        }
            
        profileDropDown.GetComponent<Dropdown>().value = ES3.Load<int>("profileSave");

        //gyroSettingsGameObject = gyroSettings.gameObject;

        axisList = new List<string>();
        for (var i = 0; i < CF2Input.activeRig.axes.list.Count; i++)
                axisList.Add(CF2Input.activeRig.axes.list[i].name);

        rightTapDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        rightDoubleTapDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        right3dTouchDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        leftTapDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        leftDoubleTapDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        left3dTouchDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        leftHammerOnDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        rightHammerOnDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        flickDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        bottomRightSwipeUpDropDown.GetComponent<Dropdown>().AddOptions(axisList);
        thrustDropDown.GetComponent<Dropdown>().AddOptions(axisList);

        buttonPositions =
            new Dictionary<string, CoOrds>();

        // Add some elements to the dictionary. There are no 
        // duplicate keys, but some of the values are duplicates.
        buttonPositions.Add("Hide", new CoOrds(100000f,100000f));
        for(var i = 0; i < buttons.Length; i++) {
            var worldPos = buttons[i].GetComponentInChildren<DynamicTouchControl>().GetWorldPos();
            buttonPositions.Add("Position " + (i+1), new CoOrds(worldPos.x, worldPos.y));
        }
        positionList = new List<string>();
        foreach (KeyValuePair<string, CoOrds> entry in buttonPositions)
        {
            positionList.Add(entry.Key);
        }

        for(var i = 0; i < buttonDropDowns.Length; i++)
            buttonDropDowns[i].GetComponent<Dropdown>().AddOptions(positionList);

        for(var i = 0; i < buttonDropDowns.Length; i++)
        {
            if (controlFile.KeyExists("button" + i + "Save"))
            {
                var buttonSave = controlFile.Load<int>("button" + i + "Save");
                buttonDropDowns[i].GetComponent<Dropdown>().value = buttonSave;
                buttonOnChange(buttonSave, i);
            }
            else
            {
                buttonDropDowns[i].GetComponent<Dropdown>().value = i + 1;
            }
        }


        if (controlFile.KeyExists("stickyButtonSave"))
        {
            var stickyButtonSave = controlFile.Load<bool>("stickyButtonSave");
            stickyButtonToggle.GetComponent<Toggle>().isOn = stickyButtonSave;
            stickyButtonToggleOnClick(stickyButtonSave);
        }
        else
            stickyButtonToggle.GetComponent<Toggle>().isOn = stickyButton.activeSelf;

        if (controlFile.KeyExists("autoFireSave"))
        {
            var autoFireSave = controlFile.Load<bool>("autoFireSave");
            autoFireToggle.GetComponent<Toggle>().isOn = autoFireSave;
            autoFireToggleOnClick(autoFireSave);
        }
        else
            autoFireToggle.GetComponent<Toggle>().isOn = aimAssistSettings.AutoFireEnabled;

        if (controlFile.KeyExists("autoFireAcquisitionTimeSave"))
        {
            var autoFireAcquisitionTimeSave = controlFile.Load<string>("autoFireAcquisitionTimeSave");
            autoFireAcquisitionTimeInputField.GetComponent<InputField>().text = autoFireAcquisitionTimeSave;
            autoFireAcquisitionTimeInputFieldOnEnd(autoFireAcquisitionTimeSave);
        }
        else
            autoFireAcquisitionTimeInputField.GetComponent<InputField>().text = aimAssistSettings.AutoFireTargetAcquisitionTime.ToString();

        if (controlFile.KeyExists("autoFireStartRadiusSave"))
        {
            var autoFireStartRadiusSave = controlFile.Load<string>("autoFireStartRadiusSave");
            autoFireStartRadiusInputField.GetComponent<InputField>().text = autoFireStartRadiusSave;
            autoFireStartRadiusInputFieldOnEnd(autoFireStartRadiusSave);
        }
        else
            autoFireStartRadiusInputField.GetComponent<InputField>().text = aimAssistSettings.AutoFireStartRadius.ToString();

        if (controlFile.KeyExists("autoFireContinueRadiusSave"))
        {
            var autoFireContinueRadiusSave = controlFile.Load<string>("autoFireContinueRadiusSave");
            autoFireContinueRadiusInputField.GetComponent<InputField>().text = autoFireContinueRadiusSave;
            autoFireContinueRadiusInputFieldOnEnd(autoFireContinueRadiusSave);
        }
        else
            autoFireContinueRadiusInputField.GetComponent<InputField>().text = aimAssistSettings.AutoFireContinueRadius.ToString();

        if (controlFile.KeyExists("autoFireMaxDistSave"))
        {
            var autoFireMaxDistSave = controlFile.Load<string>("autoFireMaxDistSave");
            autoFireMaxDistInputField.GetComponent<InputField>().text = autoFireMaxDistSave;
            autoFireMaxDistInputFieldOnEnd(autoFireMaxDistSave);
        }
        else
            autoFireMaxDistInputField.GetComponent<InputField>().text = aimAssistSettings.AutoFireDistance.ToString();

        if (controlFile.KeyExists("aimAssistHammerOnSave"))
        {
            var aimAssistHammerOnSave = controlFile.Load<bool>("aimAssistHammerOnSave");
            aimAssistHammerOnToggle.GetComponent<Toggle>().isOn = aimAssistHammerOnSave;
            aimAssistHammerOnToggleOnClick(aimAssistHammerOnSave);
        }
        else
            aimAssistHammerOnToggle.GetComponent<Toggle>().isOn = aimAssistSettings.hammerOnTarget;

        if (controlFile.KeyExists("aimSmoothingOnlyOnReleaseSave"))
        {
            var aimSmoothingOnlyOnReleaseSave = controlFile.Load<bool>("aimSmoothingOnlyOnReleaseSave");
            aimSmoothingOnlyOnReleaseToggle.GetComponent<Toggle>().isOn = aimSmoothingOnlyOnReleaseSave;
            aimSmoothingOnlyOnReleaseToggleOnClick(aimSmoothingOnlyOnReleaseSave);
        }
        else
            aimSmoothingOnlyOnReleaseToggle.GetComponent<Toggle>().isOn = aimSettings.smoothOnlyOnRelease;

        if (controlFile.KeyExists("aimAssistSave"))
        {
            var aimAssistSave = controlFile.Load<bool>("aimAssistSave");
            aimAssistToggle.GetComponent<Toggle>().isOn = aimAssistSave;
            aimAssitToggleOnClick(aimAssistSave);
        }
        else
            aimAssistToggle.GetComponent<Toggle>().isOn = aimAssistSettings.AssistedTargetingEnabled;

        if (controlFile.KeyExists("aimAssistStrengthSave"))
        {
            var aimAssistStrengthSave = controlFile.Load<string>("aimAssistStrengthSave");
            aimAssistStrengthInputField.GetComponent<InputField>().text = aimAssistStrengthSave;
            aimAssistStrengthInputFieldOnEnd(aimAssistStrengthSave);
        }
        else
            aimAssistStrengthInputField.GetComponent<InputField>().text = aimAssistSettings.AssistedTargetingCameraSpeed.ToString();

        if (controlFile.KeyExists("aimAssistStartRadiusSave"))
        {
            var aimAssistStartRadiusSave = controlFile.Load<string>("aimAssistStartRadiusSave");
            aimAssistStartRadiusInputField.GetComponent<InputField>().text = aimAssistStartRadiusSave;
            aimAssistStartRadiusInputFieldOnEnd(aimAssistStartRadiusSave);
        }
        else
            aimAssistStartRadiusInputField.GetComponent<InputField>().text = aimAssistSettings.AssistedTargetingRadius.ToString();

        if (controlFile.KeyExists("aimAssistContinueRadiusSave"))
        {
            var aimAssistContinueRadiusSave = controlFile.Load<string>("aimAssistContinueRadiusSave");
            aimAssistContinueRadiusInputField.GetComponent<InputField>().text = aimAssistContinueRadiusSave;
            aimAssistContinueRadiusInputFieldOnEnd(aimAssistContinueRadiusSave);
        }
        else
            aimAssistContinueRadiusInputField.GetComponent<InputField>().text = aimAssistSettings.AssistedTargetingTrackingRadius.ToString();

        if (controlFile.KeyExists("aimAssistMaxDistSave"))
        {
            var aimAssistMaxDistSave = controlFile.Load<string>("aimAssistMaxDistSave");
            aimAssistMaxDistInputField.GetComponent<InputField>().text = aimAssistMaxDistSave;
            aimAssistMaxDistInputFieldOnEnd(aimAssistMaxDistSave);
        }
        else
            aimAssistMaxDistInputField.GetComponent<InputField>().text = aimAssistSettings.AssistedTargetingDistance.ToString();

        if (controlFile.KeyExists("aimSensitivityHorSave"))
        {
            var aimSensitivityHorSave = controlFile.Load<string>("aimSensitivityHorSave");
            aimSensitivityHorInputField.GetComponent<InputField>().text = aimSensitivityHorSave;
            aimSensitivityHorInputFieldOnEnd(aimSensitivityHorSave);
        }
        else
            aimSensitivityHorInputField.GetComponent<InputField>().text = aimSettings.MouseLookSensitivity.x.ToString();

        if (controlFile.KeyExists("aimSensitivityVertSave"))
        {
            var aimSensitivityVertSave = controlFile.Load<string>("aimSensitivityVertSave");
            aimSensitivityVertInputField.GetComponent<InputField>().text = aimSensitivityVertSave;
            aimSensitivityVertInputFieldOnEnd(aimSensitivityVertSave);
        }
        else
            aimSensitivityVertInputField.GetComponent<InputField>().text = aimSettings.MouseLookSensitivity.y.ToString();

        if (controlFile.KeyExists("aimSensitivityHorZoomSave"))
        {
            var aimSensitivityHorZoomSave = controlFile.Load<string>("aimSensitivityHorZoomSave");
            aimSensitivityHorZoomInputField.GetComponent<InputField>().text = aimSensitivityHorZoomSave;
            aimSensitivityHorZoomInputFieldOnEnd(aimSensitivityHorZoomSave);
        }
        else
            aimSensitivityHorZoomInputField.GetComponent<InputField>().text = "1";

        if (controlFile.KeyExists("aimSensitivityVertZoomSave"))
        {
            var aimSensitivityVertZoomSave = controlFile.Load<string>("aimSensitivityVertZoomSave");
            aimSensitivityVertZoomInputField.GetComponent<InputField>().text = aimSensitivityVertZoomSave;
            aimSensitivityVertZoomInputFieldOnEnd(aimSensitivityVertZoomSave);
        }
        else
            aimSensitivityVertZoomInputField.GetComponent<InputField>().text = "1";

        if (controlFile.KeyExists("aimAccelerationSave"))
        {
            var aimAccelerationSave = controlFile.Load<bool>("aimAccelerationSave");
            aimAccelerationToggle.GetComponent<Toggle>().isOn = aimAccelerationSave;
            aimAccelerationToggleOnClick(aimAccelerationSave);
        }
        else
            aimAccelerationToggle.GetComponent<Toggle>().isOn = aimSettings.MouseLookAcceleration;

        if (controlFile.KeyExists("aimAccelerationThresholdSave"))
        {
            var aimAccelerationThresholdSave = controlFile.Load<string>("aimAccelerationThresholdSave");
            aimAccelerationThresholdInputField.GetComponent<InputField>().text = aimAccelerationThresholdSave;
            aimAccelerationThresholdInputFieldOnEnd(aimAccelerationThresholdSave);
        }
        else
            aimAccelerationThresholdInputField.GetComponent<InputField>().text = aimSettings.MouseLookAccelerationThreshold.ToString();

        if (controlFile.KeyExists("aimSmoothingStepsSave"))
        {
            var aimSmoothingStepsSave = controlFile.Load<float>("aimSmoothingStepsSave");
            
            aimSmoothingStepsSlider.GetComponent<Slider>().value = aimSmoothingStepsSave;
            Debug.Log(aimSmoothingStepsSave + " - " + aimSmoothingStepsSlider.GetComponent<Slider>().value);
            aimSmoothingStepsSliderOnEnd(aimSmoothingStepsSave);
        }
        else
            aimSmoothingStepsSlider.GetComponent<Slider>().value = aimSettings.MouseLookSmoothSteps;

        if (controlFile.KeyExists("aimSmoothingWeightSave"))
        {
            var aimSmoothingWeightSave = controlFile.Load<float>("aimSmoothingWeightSave");
            aimSmoothingWeightSlider.GetComponent<Slider>().normalizedValue = aimSmoothingWeightSave;
            aimSmoothingWeightSliderOnEnd(aimSmoothingWeightSave);
        }
        else
            aimSmoothingWeightSlider.GetComponent<Slider>().normalizedValue = aimSettings.MouseLookSmoothWeight;

        if (controlFile.KeyExists("gyroAimSave"))
        {
            var gyroAimSave = controlFile.Load<bool>("gyroAimSave");
            gyroAimToggle.GetComponent<Toggle>().isOn = gyroAimSave;
            gyroAimToggleOnClick(gyroAimSave);
        }
        else {
            //Debug.Log(gyroSettingsGameObject.activeSelf);
            //gyroSettingsGameObject.SetActive(false);
            //gyroAimToggle.GetComponent<Toggle>().isOn = gyroSettingsGameObject.activeSelf;
            FPCamSettings.SetGyroLook(false);
            gyroAimToggle.GetComponent<Toggle>().isOn = FPCamSettings.GetGyroLook();

        }

        if (controlFile.KeyExists("gyroSensitivityHorSave"))
        {
            var gyroSensitivityHorSave = controlFile.Load<string>("gyroSensitivityHorSave");
            gyroSensitivityHorInputField.GetComponent<InputField>().text = gyroSensitivityHorSave;
            gyroSensitivityHorInputFieldOnEnd(gyroSensitivityHorSave);
        }
        else
             gyroSensitivityHorInputField.GetComponent<InputField>().text = gyroSettings.gyroHeadingAmplifier.ToString();

        if (controlFile.KeyExists("gyroSensitivityVertSave"))
        {
            var gyroSensitivityVertSave = controlFile.Load<string>("gyroSensitivityVertSave");
            gyroSensitivityVertInputField.GetComponent<InputField>().text = gyroSensitivityVertSave;
            gyroSensitivityVertInputFieldOnEnd(gyroSensitivityVertSave);
        }
        else 
            gyroSensitivityVertInputField.GetComponent<InputField>().text = gyroSettings.gyroPitchAmplifier.ToString();

        if (controlFile.KeyExists("gyroSensitivityHorZoomSave"))
        {
            var gyroSensitivityHorZoomSave = controlFile.Load<string>("gyroSensitivityHorZoomSave");
            gyroSensitivityHorZoomInputField.GetComponent<InputField>().text = gyroSensitivityHorZoomSave;
            gyroSensitivityHorZoomInputFieldOnEnd(gyroSensitivityHorZoomSave);
        }
        else
            gyroSensitivityHorZoomInputField.GetComponent<InputField>().text = gyroSettings.gyroHeadingAmplifierZoom.ToString();

        if (controlFile.KeyExists("gyroSensitivityVertZoomSave"))
        {
            var gyroSensitivityVertZoomSave = controlFile.Load<string>("gyroSensitivityVertZoomSave");
            gyroSensitivityVertZoomInputField.GetComponent<InputField>().text = gyroSensitivityVertZoomSave;
            gyroSensitivityVertZoomInputFieldOnEnd(gyroSensitivityVertZoomSave);
        }
        else
            gyroSensitivityVertZoomInputField.GetComponent<InputField>().text = gyroSettings.gyroPitchAmplifierZoom.ToString();

        if (controlFile.KeyExists("gyroSmoothingSave"))
        {
            var gyroSmoothingSave = controlFile.Load<float>("gyroSmoothingSave");
            gyroSmoothingSlider.GetComponent<Slider>().value = gyroSmoothingSave;
            gyroSmoothingSliderOnEnd(gyroSmoothingSave);
        }
        else {
            gyroSmoothingSlider.GetComponent<Slider>().value = gyroSettings.smoothingTime;
        }


        if (controlFile.KeyExists("flickSave"))
        {
            var flickSave = controlFile.Load<int>("flickSave");
            flickDropDown.GetComponent<Dropdown>().value = flickSave;
            flickOnChange(flickSave);
        }
        else
            flickDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(flickSettings.targetAxis);

        if (controlFile.KeyExists("flickSpeedSave"))
        {
            var flickSpeedSave = controlFile.Load<string>("flickSpeedSave");
            flickSpeedInputField.GetComponent<InputField>().text = flickSpeedSave;
            flickSpeedInputFieldOnEnd(flickSpeedSave);
        }
        else
            flickSpeedInputField.GetComponent<InputField>().text = flickSettings.minSwipeSpeed.ToString();

        if (controlFile.KeyExists("flickDistSave"))
        {
            var flickDistSave = controlFile.Load<string>("flickDistSave");
            flickDistInputField.GetComponent<InputField>().text = flickDistSave;
            flickDistInputFieldOnEnd(flickDistSave);
        }
        else
            flickDistInputField.GetComponent<InputField>().text = flickSettings.minSwipeThresh.ToString();

        if (controlFile.KeyExists("flickTimeWindowSave"))
        {
            var flickTimeWindowSave = controlFile.Load<string>("flickTimeWindowSave");
            flickTimeWindowInputField.GetComponent<InputField>().text = flickTimeWindowSave;
            flickTimeWindowInputFieldOnEnd(flickTimeWindowSave);
        }
        else
            flickTimeWindowInputField.GetComponent<InputField>().text = flickSettings.timeWindow.ToString();

        if (controlFile.KeyExists("flickReqReleaseSave"))
        {
            var flickReqReleaseSave = controlFile.Load<bool>("flickReqReleaseSave");
            flickReqReleaseToggle.GetComponent<Toggle>().isOn = flickReqReleaseSave;
            flickReqReleaseToggleOnClick(flickReqReleaseSave);
        }
        else
            flickReqReleaseToggle.GetComponent<Toggle>().isOn = flickSettings.requireSwipeRelease;

        if (controlFile.KeyExists("flickStartFromCenterSave"))
        {
            var flickStartFromCenterSave = controlFile.Load<bool>("flickStartFromCenterSave");
            flickStartFromCenterToggle.GetComponent<Toggle>().isOn = flickStartFromCenterSave;
            flickStartFromCenterToggleOnClick(flickStartFromCenterSave);
        }
        else
            flickStartFromCenterToggle.GetComponent<Toggle>().isOn = flickSettings.requireSwipeFromCenter;

        if (controlFile.KeyExists("rightTapSave"))
        {
            var rightTapSave = controlFile.Load<int>("rightTapSave");
            rightTapDropDown.GetComponent<Dropdown>().value = rightTapSave;
            rightTapOnChange(rightTapSave);
        }
        else
            rightTapDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(rightSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].axisName);

        if (controlFile.KeyExists("rightDoubleTapSave"))
        {
            var rightDoubleTapSave = controlFile.Load<int>("rightDoubleTapSave");
            rightDoubleTapDropDown.GetComponent<Dropdown>().value = rightDoubleTapSave;
            rightDoubleTapOnChange(rightDoubleTapSave);
        }
        else
            rightDoubleTapDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(rightDoubleTapSettings.targetAxis);

        if (controlFile.KeyExists("right3dTouchSave"))
        {
            var right3dTouchSave = controlFile.Load<int>("right3dTouchSave");
            right3dTouchDropDown.GetComponent<Dropdown>().value = right3dTouchSave;
            right3dTouchOnChange(right3dTouchSave);
        }
        else
            right3dTouchDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(rightPressureSettings.rangeConfigList[0].axisTarget);

        if (controlFile.KeyExists("right3dTouchPressureSave"))
        {
            var right3dTouchPressureSave = controlFile.Load<float>("right3dTouchPressureSave");
            right3dTouchPressureSlider.GetComponent<Slider>().normalizedValue = right3dTouchPressureSave;
            right3dTouchPressureSliderOnEnd(right3dTouchPressureSave);
        }
        else
            right3dTouchPressureSlider.GetComponent<Slider>().normalizedValue = rightPressureSettings.rangeConfigList[0].min;

        if (controlFile.KeyExists("leftTapSave"))
        {
            var leftTapSave = controlFile.Load<int>("leftTapSave");
            leftTapDropDown.GetComponent<Dropdown>().value = leftTapSave;
            leftTapOnChange(leftTapSave);
        }
        else
            leftTapDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(leftSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].axisName);

        if (controlFile.KeyExists("leftDoubleTapSave"))
        {
            var leftDoubleTapSave = controlFile.Load<int>("leftDoubleTapSave");
            leftDoubleTapDropDown.GetComponent<Dropdown>().value = leftDoubleTapSave;
            leftDoubleTapOnChange(leftDoubleTapSave);
        }
        else
            leftDoubleTapDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(leftDoubleTapSettings.targetAxis);


        if (controlFile.KeyExists("left3dTouchSave"))
        {
            var left3dTouchSave = controlFile.Load<int>("left3dTouchSave");
            left3dTouchDropDown.GetComponent<Dropdown>().value = left3dTouchSave;
            left3dTouchOnChange(left3dTouchSave);
        }
        else
            left3dTouchDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(leftPressureSettings.rangeConfigList[0].axisTarget);



        if (controlFile.KeyExists("left3dTouchPressureSave"))
        {
            var left3dTouchPressureSave = controlFile.Load<float>("left3dTouchPressureSave");
            left3dTouchPressureSlider.GetComponent<Slider>().normalizedValue = left3dTouchPressureSave;
            left3dTouchPressureSliderOnEnd(left3dTouchPressureSave);
        }
        else
            left3dTouchPressureSlider.GetComponent<Slider>().normalizedValue = leftPressureSettings.rangeConfigList[0].min;

        if (controlFile.KeyExists("leftHammerOnSave"))
        {
            var leftHammerOnSave = controlFile.Load<int>("leftHammerOnSave");
            leftHammerOnDropDown.GetComponent<Dropdown>().value = leftHammerOnSave;
            leftHammerOnOnChange(leftHammerOnSave);
        }
        else
            leftHammerOnDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(leftHammerOnSettings.targetAxis);

        if (controlFile.KeyExists("leftHammerOnMaxGapSave"))
        {
            var leftHammerOnMaxGapSave = controlFile.Load<string>("leftHammerOnMaxGapSave");
            leftHammerOnMaxGapInputField.GetComponent<InputField>().text = leftHammerOnMaxGapSave;
            leftHammerOnMaxGapInputFieldOnEnd(leftHammerOnMaxGapSave);
        }
        else
            leftHammerOnMaxGapInputField.GetComponent<InputField>().text = leftHammerOnSettings.hammerOnGap.ToString();

        if (controlFile.KeyExists("leftHammerOnMaxDistSave"))
        {
            var leftHammerOnMaxDistSave = controlFile.Load<string>("leftHammerOnMaxDistSave");
            leftHammerOnMaxDistInputField.GetComponent<InputField>().text = leftHammerOnMaxDistSave;
            leftHammerOnMaxDistInputFieldOnEnd(leftHammerOnMaxDistSave);
        }
        else
            leftHammerOnMaxDistInputField.GetComponent<InputField>().text = leftHammerOnSettings.hammerOnMaxDist.ToString();

        if (controlFile.KeyExists("rightHammerOnSave"))
        {
            var rightHammerOnSave = controlFile.Load<int>("rightHammerOnSave");
            rightHammerOnDropDown.GetComponent<Dropdown>().value = rightHammerOnSave;
            rightHammerOnOnChange(rightHammerOnSave);
        }
        else
            rightHammerOnDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(rightHammerOnSettings.targetAxis);

        if (controlFile.KeyExists("rightHammerOnMaxGapSave"))
        {
            var rightHammerOnMaxGapSave = controlFile.Load<string>("rightHammerOnMaxGapSave");
            rightHammerOnMaxGapInputField.GetComponent<InputField>().text = rightHammerOnMaxGapSave;
            rightHammerOnMaxGapInputFieldOnEnd(rightHammerOnMaxGapSave);
        }
        else
            rightHammerOnMaxGapInputField.GetComponent<InputField>().text = rightHammerOnSettings.hammerOnGap.ToString();

        if (controlFile.KeyExists("rightHammerOnMaxDistSave"))
        {
            var rightHammerOnMaxDistSave = controlFile.Load<string>("rightHammerOnMaxDistSave");
            rightHammerOnMaxDistInputField.GetComponent<InputField>().text = rightHammerOnMaxDistSave;
            rightHammerOnMaxDistInputFieldOnEnd(rightHammerOnMaxDistSave);
        }
        else
            rightHammerOnMaxDistInputField.GetComponent<InputField>().text = rightHammerOnSettings.hammerOnMaxDist.ToString();

        if (controlFile.KeyExists("bottomRightSwipeUpSave"))
        {
            var bottomRightSwipeUpSave = controlFile.Load<int>("bottomRightSwipeUpSave");
            bottomRightSwipeUpDropDown.GetComponent<Dropdown>().value = bottomRightSwipeUpSave;
            bottomRightSwipeUpOnChange(bottomRightSwipeUpSave);
        }
        else
            bottomRightSwipeUpDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(bottomRightSettings.multiFingerConfigs[0].binding.normalPressSwipeDirBinding.dirBindingU.axisList[0].axisName);
        //bottomRightSwipeUpDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(bottomRightSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].axisName);

        if (controlFile.KeyExists("thrustSave"))
        {
            var thrustSave = controlFile.Load<int>("thrustSave");
            thrustDropDown.GetComponent<Dropdown>().value = thrustSave;
            thrustOnChange(thrustSave);
        }
        else
            thrustDropDown.GetComponent<Dropdown>().value = axisList.IndexOf(thrustSettings.targetAxis);

        if (controlFile.KeyExists("thrustThresholdSave"))
        {
            var thrustThresholdSave = controlFile.Load<string>("thrustThresholdSave");
            thrustThresholdInputField.GetComponent<InputField>().text = thrustThresholdSave;
            thrustThresholdInputFieldOnEnd(thrustThresholdSave);
        }
        else
            thrustThresholdInputField.GetComponent<InputField>().text = thrustSettings.accelerationThreshold.ToString();

        if (controlFile.KeyExists("dynamicJoystickSave"))
        {
            var dynamicJoystickSave = controlFile.Load<bool>("dynamicJoystickSave");
            dynamicJoystickToggle.GetComponent<Toggle>().isOn = dynamicJoystickSave;
            dynamicJoystickToggleOnClick(dynamicJoystickSave);
        }
        else
            dynamicJoystickToggle.GetComponent<Toggle>().isOn = joystickSettings.GetDynamicRegion();

        if (controlFile.KeyExists("stickyJoystickSave"))
        {
            var stickyJoystickSave = controlFile.Load<bool>("stickyJoystickSave");
            stickyJoystickToggle.GetComponent<Toggle>().isOn = stickyJoystickSave;
            stickyJoystickToggleOnClick(stickyJoystickSave);
        }
        else
            stickyJoystickToggle.GetComponent<Toggle>().isOn = joystickSettings.stickyMode;

        if (controlFile.KeyExists("minAnalogRangeSave"))
        {
            var minAnalogRangeSave = controlFile.Load<float>("minAnalogRangeSave");
            minAnalogRangeSlider.GetComponent<Slider>().value = minAnalogRangeSave;
            minAnalogRangeSliderOnEnd(minAnalogRangeSave);
        }
        else
            minAnalogRangeSlider.GetComponent<Slider>().value = joystickSettings.config.analogDeadZone;

        if (controlFile.KeyExists("maxAnalogRangeSave"))
        {
            var maxAnalogRangeSave = controlFile.Load<float>("maxAnalogRangeSave");
            maxAnalogRangeSlider.GetComponent<Slider>().value = maxAnalogRangeSave;
            maxAnalogRangeSliderOnEnd(maxAnalogRangeSave);
        }
        else
            maxAnalogRangeSlider.GetComponent<Slider>().value = joystickSettings.config.analogEndZone;

        if (controlFile.KeyExists("startValueAnalogRangeSave"))
        {
            var startValueAnalogRangeSave = controlFile.Load<string>("startValueAnalogRangeSave");
            startValueAnalogRangeInputField.GetComponent<InputField>().text = startValueAnalogRangeSave;
            startValueAnalogRangeInputFieldOnEnd(startValueAnalogRangeSave);
        }
        else
            startValueAnalogRangeInputField.GetComponent<InputField>().text = joystickSettings.config.analogRangeStartValue.ToString();


        if (controlFile.KeyExists("showBaseSave"))
        {
            var showBaseSave = controlFile.Load<bool>("showBaseSave");
            showBaseToggle.GetComponent<Toggle>().isOn = showBaseSave;
            showBaseToggleOnClick(showBaseSave);
        }
        else
            showBaseToggle.GetComponent<Toggle>().isOn = baseObj.activeInHierarchy;

        if (controlFile.KeyExists("showHatSave"))
        {
            var showHatSave = controlFile.Load<bool>("showHatSave");
            showHatToggle.GetComponent<Toggle>().isOn = showHatSave;
            showHatToggleOnClick(showHatSave);
        }
        else
            showHatToggle.GetComponent<Toggle>().isOn = hatObj.activeInHierarchy;

        if (controlFile.KeyExists("showDirectionSave"))
        {
            var showDirectionSave = controlFile.Load<bool>("showDirectionSave");
            showDirectionToggle.GetComponent<Toggle>().isOn = showDirectionSave;
            showDirectionToggleOnClick(showDirectionSave);
        }
        else
            showDirectionToggle.GetComponent<Toggle>().isOn = directionObj.activeInHierarchy;




        gameObject.SetActive(false); //turn off menu
        initBool = true;

    }

    private void OnEnable()
    {
        ControlLayer1.SetActive(false);
    }

    private void OnDisable()
    {
        ControlLayer1.SetActive(true);
    }

    private void CopyValues()
    {
        Debug.Log("Copy Values");

        for (var i = 0; i < buttonDropDowns.Length; i++)
        {
            if (dirtySettings.Contains("button" + i + "Save"))
                buttonOnChange(buttonDropDowns[i].GetComponent<Dropdown>().value, i);
        }


        if (dirtySettings.Contains("stickyButtonSave"))
            stickyButtonToggleOnClick(stickyButtonToggle.GetComponent<Toggle>().isOn);


        if (dirtySettings.Contains("autoFireSave"))
            autoFireToggleOnClick(autoFireToggle.GetComponent<Toggle>().isOn);


        if (dirtySettings.Contains("autoFireStartRadiusSave"))
            autoFireStartRadiusInputFieldOnEnd(autoFireStartRadiusInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("autoFireContinueRadiusSave"))
            autoFireContinueRadiusInputFieldOnEnd(autoFireContinueRadiusInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("autoFireMaxDistSave"))
            autoFireMaxDistInputFieldOnEnd(autoFireMaxDistInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimAssistStartRadiusSave"))
            aimAssistStartRadiusInputFieldOnEnd(aimAssistStartRadiusInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimAssistContinueRadiusSave"))
            aimAssistContinueRadiusInputFieldOnEnd(aimAssistContinueRadiusInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimAssistMaxDistSave"))
            aimAssistMaxDistInputFieldOnEnd(aimAssistMaxDistInputField.GetComponent<InputField>().text);


        if (dirtySettings.Contains("autoFireAcquisitionTimeSave"))
            autoFireAcquisitionTimeInputFieldOnEnd(autoFireAcquisitionTimeInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimAssistSave"))
            aimAssitToggleOnClick(aimAssistToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("aimAssistStrengthSave"))
            aimAssistStrengthInputFieldOnEnd(aimAssistStrengthInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimSensitivityHorSave"))
            aimSensitivityHorInputFieldOnEnd(aimSensitivityHorInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimSensitivityVertSave"))
            aimSensitivityVertInputFieldOnEnd(aimSensitivityVertInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimSensitivityHorZoomSave"))
            gyroSensitivityHorZoomInputFieldOnEnd(gyroSensitivityHorZoomInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimSensitivityVertZoomSave"))
            gyroSensitivityVertZoomInputFieldOnEnd(gyroSensitivityVertZoomInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimAccelerationSave"))
            aimAccelerationToggleOnClick(aimAccelerationToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("aimAccelerationThresholdSave"))
            aimAccelerationThresholdInputFieldOnEnd(aimAccelerationThresholdInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("aimSmoothingStepsSave"))
            aimSmoothingStepsSliderOnEnd(aimSmoothingStepsSlider.GetComponent<Slider>().value);
        if (dirtySettings.Contains("aimSmoothingWeightSave"))
            aimSmoothingWeightSliderOnEnd(aimSmoothingWeightSlider.GetComponent<Slider>().normalizedValue);
        if (dirtySettings.Contains("gyroAimSave"))
            gyroAimToggleOnClick(gyroAimToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("gyroSensitivityHorSave"))
            gyroSensitivityHorInputFieldOnEnd(gyroSensitivityHorInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("gyroSensitivityVertSave"))
            gyroSensitivityVertInputFieldOnEnd(gyroSensitivityVertInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("gyroSensitivityHorZoomSave"))
            aimSensitivityHorZoomInputFieldOnEnd(aimSensitivityHorZoomInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("gyroSensitivityVertZoomSave"))
            aimSensitivityVertZoomInputFieldOnEnd(aimSensitivityVertZoomInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("gyroSmoothingSave"))
            gyroSmoothingSliderOnEnd(gyroSmoothingSlider.GetComponent<Slider>().value);
        if (dirtySettings.Contains("flickSave"))
            flickOnChange(flickDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("flickSpeedSave"))
            flickSpeedInputFieldOnEnd(flickSpeedInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("flickDistSave"))
            flickDistInputFieldOnEnd(flickDistInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("flickTimeWindowSave"))
            flickTimeWindowInputFieldOnEnd(flickTimeWindowInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("flickReqReleaseSave"))
            flickReqReleaseToggleOnClick(flickReqReleaseToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("flickStartFromCenterSave"))
            flickStartFromCenterToggleOnClick(flickStartFromCenterToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("rightTapSave"))
            rightTapOnChange(rightTapDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("right3dTouchSave"))
            right3dTouchOnChange(right3dTouchDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("right3dTouchPressureSave"))
            right3dTouchPressureSliderOnEnd(right3dTouchPressureSlider.GetComponent<Slider>().normalizedValue);
        if (dirtySettings.Contains("leftTapSave"))
            leftTapOnChange(leftTapDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("left3dTouchSave"))
            left3dTouchOnChange(left3dTouchDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("left3dTouchPressureSave"))
            left3dTouchPressureSliderOnEnd(left3dTouchPressureSlider.GetComponent<Slider>().normalizedValue);
        if (dirtySettings.Contains("leftHammerOnSave"))
            leftHammerOnOnChange(leftHammerOnDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("leftHammerOnMaxGapSave"))
            leftHammerOnMaxGapInputFieldOnEnd(leftHammerOnMaxGapInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("leftHammerOnMaxDistSave"))
            leftHammerOnMaxDistInputFieldOnEnd(leftHammerOnMaxDistInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("rightHammerOnSave"))
            rightHammerOnOnChange(rightHammerOnDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("rightHammerOnMaxGapSave"))
            rightHammerOnMaxGapInputFieldOnEnd(rightHammerOnMaxGapInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("rightHammerOnMaxDistSave"))
            rightHammerOnMaxDistInputFieldOnEnd(rightHammerOnMaxDistInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("bottomRightSwipeUpSave"))
            bottomRightSwipeUpOnChange(bottomRightSwipeUpDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("thrustSave"))
            thrustOnChange(thrustDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("thrustThresholdSave"))
            thrustThresholdInputFieldOnEnd(thrustThresholdInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("dynamicJoystickSave"))
            dynamicJoystickToggleOnClick(dynamicJoystickToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("stickyJoystickSave"))
            stickyJoystickToggleOnClick(stickyJoystickToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("minAnalogRangeSave"))
            minAnalogRangeSliderOnEnd(minAnalogRangeSlider.GetComponent<Slider>().value);
        if (dirtySettings.Contains("maxAnalogRangeSave"))
            maxAnalogRangeSliderOnEnd(maxAnalogRangeSlider.GetComponent<Slider>().value);
        if (dirtySettings.Contains("startValueAnalogRangeSave"))
            startValueAnalogRangeInputFieldOnEnd(startValueAnalogRangeInputField.GetComponent<InputField>().text);
        if (dirtySettings.Contains("showBaseSave"))
            showBaseToggleOnClick(showBaseToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("showHatSave"))
            showHatToggleOnClick(showHatToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("showDirectionSave"))
            showDirectionToggleOnClick(showDirectionToggle.GetComponent<Toggle>().isOn);

        if (dirtySettings.Contains("rightDoubleTapSave"))
            rightDoubleTapOnChange(rightDoubleTapDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("leftDoubleTapSave"))
            leftDoubleTapOnChange(leftDoubleTapDropDown.GetComponent<Dropdown>().value);
        if (dirtySettings.Contains("aimAssistHammerOnSave"))
            aimAssistHammerOnToggleOnClick(aimAssistHammerOnToggle.GetComponent<Toggle>().isOn);
        if (dirtySettings.Contains("aimSmoothingOnlyOnReleaseSave"))
            aimSmoothingOnlyOnReleaseToggleOnClick(aimSmoothingOnlyOnReleaseToggle.GetComponent<Toggle>().isOn);


    }

    private bool CustomSwitch()
    {
        
        
        if (profileList[ES3.Load<int>("profileSave")] != "Custom" && initBool)
        {
            Debug.Log("Save");
            initBool = false;
            profileDropDown.GetComponent<Dropdown>().value = profileList.IndexOf("Custom");
            initBool = true;
            controlFile = new ES3File("SaveData.es3");
            CopyValues();
            return true;
        }
        else if(profileList[ES3.Load<int>("profileSave")] == "Custom")
        {
            return true;
        }
        return false;

    }


    public void profileOnChange(int profileSave)
    {
        if (initBool && profileList[profileSave] == "Custom" && !ES3.KeyExists("rightHammerOnSave"))
        { //if profile changed to "custom" and file is empty, copy previous profile
            Debug.Log("Copy File");
            //var localFileSettings = new ES3Settings();
            //localFileSettings.location = ES3.Location.File;
            //var es3SettingsOld = es3Settings;
            //es3Settings.location = ES3.Location.File;
            //Debug.Log(es3Settings.FullPath);

            //ES3.CopyFile(profileList[ES3.Load<int>("profileSave")], "SaveData.es3", es3Settings, localFileSettings);

            
            ES3.Save<int>("profileSave", profileSave);
            controlFile = new ES3File("SaveData.es3");
            CopyValues();
        }
        else
        {
            ES3.Save<int>("profileSave", profileSave);

            if (initBool)
            { //don't reload scene on initialization
                Debug.Log("Reload Scene");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }


    }

    public void crouchButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 0);
    }

    public void jumpButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 1);
    }

    public void quickShotButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 2);
    }

    public void snipeButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 3);
    }

    public void attackButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 4);
    }

    public void quickMeleeButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 5);
    }

    public void flashBangButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 6);
    }

    public void stickyQuickSHotButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 7);
    }

    public void combatRollButtonOnChange(int buttonSave)
    {
        buttonOnChange(buttonSave, 8);
    }







    public void buttonOnChange(int valueSave, int index)
    {
        if (buttons[index].GetComponentInChildren<DynamicTouchControl>().GetWorldPos() != new Vector3(buttonPositions[positionList[valueSave]].x, buttonPositions[positionList[valueSave]].y, buttons[index].GetComponentInChildren<DynamicTouchControl>().GetWorldPos().z))
        {

            if (!dirtySettings.Contains("button" + index + "Save"))
                dirtySettings.Add("button" + index + "Save");

            buttons[index].GetComponentInChildren<DynamicTouchControl>().SetWorldPos(new Vector2(buttonPositions[positionList[valueSave]].x, buttonPositions[positionList[valueSave]].y));
            //buttons[index].position = new Vector3(buttonPositions[positionList[valueSave]].x, buttonPositions[positionList[valueSave]].y, buttons[index].position.z);
        }
        if (CustomSwitch())
        {
            controlFile.Save<int>("button" + index + "Save", valueSave);
            controlFile.Sync();
        }
        
    }

    

    public void stickyButtonToggleOnClick(bool stickyButtonSave)
    {
        if (stickyButton.activeSelf != stickyButtonSave)
        {
            if (!dirtySettings.Contains("stickyButtonSave"))
                dirtySettings.Add("stickyButtonSave");

            stickyButton.SetActive(stickyButtonSave);
        }
        if (CustomSwitch())
        {
            controlFile.Save<bool>("stickyButtonSave", stickyButtonSave);
            controlFile.Sync();
        }

    }

    public void autoFireToggleOnClick(bool autoFireSave)
    {
        if (aimAssistSettings.AutoFireEnabled != autoFireSave)
        {
            if (!dirtySettings.Contains("autoFireSave"))
                dirtySettings.Add("autoFireSave");

            aimAssistSettings.AutoFireEnabled = autoFireSave;
        }
        if (CustomSwitch())
        {
            controlFile.Save<bool>("autoFireSave", autoFireSave);
            controlFile.Sync();
        }
    
    }

    public void autoFireAcquisitionTimeInputFieldOnEnd(string autoFireAcquisitionTimeSave)
    {
        if (aimAssistSettings.AutoFireTargetAcquisitionTime != float.Parse(autoFireAcquisitionTimeSave))
        {
            if (!dirtySettings.Contains("autoFireAcquisitionTimeSave"))
                dirtySettings.Add("autoFireAcquisitionTimeSave");

            aimAssistSettings.AutoFireTargetAcquisitionTime = float.Parse(autoFireAcquisitionTimeSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("autoFireAcquisitionTimeSave", autoFireAcquisitionTimeSave);
            controlFile.Sync();
        }
    }

    public void autoFireStartRadiusInputFieldOnEnd(string autoFireStartRadiusSave)
    {
        if (aimAssistSettings.AutoFireStartRadius != float.Parse(autoFireStartRadiusSave))
        {
            if (!dirtySettings.Contains("autoFireStartRadiusSave"))
                dirtySettings.Add("autoFireStartRadiusSave");

            aimAssistSettings.AutoFireStartRadius = float.Parse(autoFireStartRadiusSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("autoFireStartRadiusSave", autoFireStartRadiusSave);
            controlFile.Sync();
        }
    }

    public void autoFireContinueRadiusInputFieldOnEnd(string autoFireContinueRadiusSave)
    {
        if (aimAssistSettings.AutoFireContinueRadius != float.Parse(autoFireContinueRadiusSave))
        {
            if (!dirtySettings.Contains("autoFireContinueRadiusSave"))
                dirtySettings.Add("autoFireContinueRadiusSave");

            aimAssistSettings.AutoFireContinueRadius = float.Parse(autoFireContinueRadiusSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("autoFireContinueRadiusSave", autoFireContinueRadiusSave);
            controlFile.Sync();
        }
    }

    public void autoFireMaxDistInputFieldOnEnd(string autoFireMaxDistSave)
    {
        if (aimAssistSettings.AutoFireDistance != float.Parse(autoFireMaxDistSave))
        {
            if (!dirtySettings.Contains("autoFireMaxDistSave"))
                dirtySettings.Add("autoFireMaxDistSave");

            aimAssistSettings.AutoFireDistance = float.Parse(autoFireMaxDistSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("autoFireMaxDistSave", autoFireMaxDistSave);
            controlFile.Sync();
        }
    }

    public void aimAssistStartRadiusInputFieldOnEnd(string aimAssistStartRadiusSave)
    {
        if (aimAssistSettings.AssistedTargetingRadius != float.Parse(aimAssistStartRadiusSave))
        {
            if (!dirtySettings.Contains("aimAssistStartRadiusSave"))
                dirtySettings.Add("aimAssistStartRadiusSave");

            aimAssistSettings.AssistedTargetingRadius = float.Parse(aimAssistStartRadiusSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimAssistStartRadiusSave", aimAssistStartRadiusSave);
            controlFile.Sync();
        }
    }

    public void aimAssistContinueRadiusInputFieldOnEnd(string aimAssistContinueRadiusSave)
    {
        if (aimAssistSettings.AssistedTargetingTrackingRadius != float.Parse(aimAssistContinueRadiusSave))
        {
            if (!dirtySettings.Contains("aimAssistContinueRadiusSave"))
                dirtySettings.Add("aimAssistContinueRadiusSave");

            aimAssistSettings.AssistedTargetingTrackingRadius = float.Parse(aimAssistContinueRadiusSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimAssistContinueRadiusSave", aimAssistContinueRadiusSave);
            controlFile.Sync();
        }
    }

    public void aimAssistMaxDistInputFieldOnEnd(string aimAssistMaxDistSave)
    {
        if (aimAssistSettings.AssistedTargetingDistance != float.Parse(aimAssistMaxDistSave))
        {
            if (!dirtySettings.Contains("aimAssistMaxDistSave"))
                dirtySettings.Add("aimAssistMaxDistSave");

            aimAssistSettings.AssistedTargetingDistance = float.Parse(aimAssistMaxDistSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimAssistMaxDistSave", aimAssistMaxDistSave);
            controlFile.Sync();
        }
    }
    

    public void aimAssistHammerOnToggleOnClick(bool aimAssistHammerOnSave)
    {
        if(aimAssistSettings.hammerOnTarget != aimAssistHammerOnSave)
        {
            if (!dirtySettings.Contains("aimAssistHammerOnSave"))
                dirtySettings.Add("aimAssistHammerOnSave");

            aimAssistSettings.hammerOnTarget = aimAssistHammerOnSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("aimAssistHammerOnSave", aimAssistHammerOnSave);
            controlFile.Sync();
        }
    }

    public void aimAssitToggleOnClick(bool aimAssistSave)
    {

        if (aimAssistSettings.AssistedTargetingEnabled != aimAssistSave)
        {
            if (!dirtySettings.Contains("aimAssistSave"))
                dirtySettings.Add("aimAssistSave");

            aimAssistSettings.AssistedTargetingEnabled = aimAssistSave;
        }
            
        if (CustomSwitch())
        {
            controlFile.Save<bool>("aimAssistSave", aimAssistSave);
            controlFile.Sync();
        }
    }

    public void aimAssistStrengthInputFieldOnEnd(string aimAssistStrengthSave)
    {
        if(aimAssistSettings.AssistedTargetingCameraSpeed != float.Parse(aimAssistStrengthSave))
        {
            if (!dirtySettings.Contains("aimAssistStrengthSave"))
                dirtySettings.Add("aimAssistStrengthSave");

            aimAssistSettings.AssistedTargetingCameraSpeed = float.Parse(aimAssistStrengthSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimAssistStrengthSave", aimAssistStrengthSave);
            controlFile.Sync();
        }
    }

    public void aimSensitivityHorInputFieldOnEnd(string aimSensitivityHorSave)
    {

        if(aimSettings.MouseLookSensitivity.x != float.Parse(aimSensitivityHorSave))
        {
            if (!dirtySettings.Contains("aimSensitivityHorSave"))
                dirtySettings.Add("aimSensitivityHorSave");

            aimSettings.MouseLookSensitivity.x = float.Parse(aimSensitivityHorSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimSensitivityHorSave", aimSensitivityHorSave);
            controlFile.Sync();
        }
    }

    public void aimSensitivityVertInputFieldOnEnd(string aimSensitivityVertSave)
    {
        if(aimSettings.MouseLookSensitivity.y != float.Parse(aimSensitivityVertSave))
        {
            if (!dirtySettings.Contains("aimSensitivityVertSave"))
                dirtySettings.Add("aimSensitivityVertSave");

            aimSettings.MouseLookSensitivity.y = float.Parse(aimSensitivityVertSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimSensitivityVertSave", aimSensitivityVertSave);
            controlFile.Sync();
        }
    }

    public void aimSensitivityHorZoomInputFieldOnEnd(string aimSensitivityHorZoomSave)
    {

        if (aimSettings.MouseLookSensitivityZoom.x != float.Parse(aimSensitivityHorZoomSave))
        {
            if (!dirtySettings.Contains("aimSensitivityHorZoomSave"))
                dirtySettings.Add("aimSensitivityHorZoomSave");

            aimSettings.MouseLookSensitivityZoom.x = float.Parse(aimSensitivityHorZoomSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimSensitivityHorZoomSave", aimSensitivityHorZoomSave);
            controlFile.Sync();
        }
    }

    public void aimSensitivityVertZoomInputFieldOnEnd(string aimSensitivityVertZoomSave)
    {
        if (aimSettings.MouseLookSensitivityZoom.y != float.Parse(aimSensitivityVertZoomSave))
        {
            if (!dirtySettings.Contains("aimSensitivityVertZoomSave"))
                dirtySettings.Add("aimSensitivityVertZoomSave");

            aimSettings.MouseLookSensitivityZoom.y = float.Parse(aimSensitivityVertZoomSave);
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("aimSensitivityVertZoomSave", aimSensitivityVertZoomSave);
            controlFile.Sync();
        }
    }

    public void aimAccelerationToggleOnClick(bool aimAccelerationSave)
    {
        if(aimSettings.MouseLookAcceleration != aimAccelerationSave)
        {
            if (!dirtySettings.Contains("aimAccelerationSave"))
                dirtySettings.Add("aimAccelerationSave");

            aimSettings.MouseLookAcceleration = aimAccelerationSave;
        }

        if (CustomSwitch())
        {
            controlFile.Save<bool>("aimAccelerationSave", aimAccelerationSave);
            controlFile.Sync();
        }
    }

    public void aimAccelerationThresholdInputFieldOnEnd(string aimAccelerationThresholdSave)
    {
        if(aimSettings.MouseLookAccelerationThreshold != float.Parse(aimAccelerationThresholdSave))
        {
            if (!dirtySettings.Contains("aimAccelerationThresholdSave"))
                dirtySettings.Add("aimAccelerationThresholdSave");

            aimSettings.MouseLookAccelerationThreshold = float.Parse(aimAccelerationThresholdSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("aimAccelerationThresholdSave", aimAccelerationThresholdSave);
            controlFile.Sync();
        }
    }

    public void aimSmoothingStepsSliderOnEnd(float aimSmoothingStepsSave)
    {
        if(aimSettings.MouseLookSmoothSteps != (int)aimSmoothingStepsSave)
        {
            if (!dirtySettings.Contains("aimSmoothingStepsSave"))
                dirtySettings.Add("aimSmoothingStepsSave");

            aimSettings.MouseLookSmoothSteps = (int)aimSmoothingStepsSave;

        }

        if (CustomSwitch())
        {
            controlFile.Save<float>("aimSmoothingStepsSave", aimSmoothingStepsSave);
            controlFile.Sync();
        }
    }

    public void aimSmoothingWeightSliderOnEnd(float aimSmoothingWeightSave)
    {
        if(aimSettings.MouseLookSmoothWeight != aimSmoothingWeightSave)
        {
            if (!dirtySettings.Contains("aimSmoothingWeightSave"))
                dirtySettings.Add("aimSmoothingWeightSave");

            aimSettings.MouseLookSmoothWeight = aimSmoothingWeightSave;
        }
       
        if (CustomSwitch())
        {
            controlFile.Save<float>("aimSmoothingWeightSave", aimSmoothingWeightSave);
            controlFile.Sync();
        }
    }

    public void aimSmoothingOnlyOnReleaseToggleOnClick(bool aimSmoothingOnlyOnReleaseSave)
    {
        if(aimSettings.smoothOnlyOnRelease != aimSmoothingOnlyOnReleaseSave)
        {
            if (!dirtySettings.Contains("aimSmoothingOnlyOnReleaseSave"))
                dirtySettings.Add("aimSmoothingOnlyOnReleaseSave");

            aimSettings.smoothOnlyOnRelease = aimSmoothingOnlyOnReleaseSave;
        }

        if (CustomSwitch())
        {
            controlFile.Save<bool>("aimSmoothingOnlyOnReleaseSave", aimSmoothingOnlyOnReleaseSave);
            controlFile.Sync();
        }
    }

    public void gyroAimToggleOnClick(bool gyroAimSave)
    {
        if (FPCamSettings.GetGyroLook() != gyroAimSave)
        {
            if (!dirtySettings.Contains("gyroAimSave"))
                dirtySettings.Add("gyroAimSave");

            FPCamSettings.SetGyroLook(gyroAimSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("gyroAimSave", gyroAimSave);
            controlFile.Sync();
        }
    }

    public void gyroSensitivityHorInputFieldOnEnd(string gyroSensitivityHorSave)
    {
        if(GyroAccel.GetGyroHeadingAmplifier() != float.Parse(gyroSensitivityHorSave))
        {
            if (!dirtySettings.Contains("gyroSensitivityHorSave"))
                dirtySettings.Add("gyroSensitivityHorSave");

            GyroAccel.SetGyroHeadingAmplifier(float.Parse(gyroSensitivityHorSave));
        }

        
        //gyroSettings.gyroHeadingAmplifier = float.Parse(gyroSensitivityHorSave);
        if (CustomSwitch())
        {
            controlFile.Save<string>("gyroSensitivityHorSave", gyroSensitivityHorSave);
            controlFile.Sync();
        }
    }

    public void gyroSensitivityVertInputFieldOnEnd(string gyroSensitivityVertSave)
    {
        if(GyroAccel.GetGyroPitchAmplifier() != float.Parse(gyroSensitivityVertSave))
        {
            if (!dirtySettings.Contains("gyroSensitivityVertSave"))
                dirtySettings.Add("gyroSensitivityVertSave");

            GyroAccel.SetGyroPitchAmplifier(float.Parse(gyroSensitivityVertSave));
        }

        
        //gyroSettings.gyroPitchAmplifier = float.Parse(gyroSensitivityVertSave);
        if (CustomSwitch())
        {
            controlFile.Save<string>("gyroSensitivityVertSave", gyroSensitivityVertSave);
            controlFile.Sync();
        }
    }

    public void gyroSensitivityHorZoomInputFieldOnEnd(string gyroSensitivityHorZoomSave)
    {
        if (GyroAccel.GetGyroHeadingAmplifierZoom() != float.Parse(gyroSensitivityHorZoomSave))
        {
            if (!dirtySettings.Contains("gyroSensitivityHorZoomSave"))
                dirtySettings.Add("gyroSensitivityHorZoomSave");

            GyroAccel.SetGyroHeadingAmplifierZoom(float.Parse(gyroSensitivityHorZoomSave));
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("gyroSensitivityHorZoomSave", gyroSensitivityHorZoomSave);
            controlFile.Sync();
        }
    }

    public void gyroSensitivityVertZoomInputFieldOnEnd(string gyroSensitivityVertZoomSave)
    {
        if (GyroAccel.GetGyroPitchAmplifierZoom() != float.Parse(gyroSensitivityVertZoomSave))
        {
            if (!dirtySettings.Contains("gyroSensitivityVertZoomSave"))
                dirtySettings.Add("gyroSensitivityVertZoomSave");

            GyroAccel.SetGyroPitchAmplifierZoom(float.Parse(gyroSensitivityVertZoomSave));
        }

        if (CustomSwitch())
        {
            controlFile.Save<string>("gyroSensitivityVertZoomSave", gyroSensitivityVertZoomSave);
            controlFile.Sync();
        }
    }


    public void gyroSmoothingSliderOnEnd(float gyroSmoothingSave)
    {
        if(GyroAccel.GetSmoothingTime() != gyroSmoothingSave)
        {
            if (!dirtySettings.Contains("gyroSmoothingSave"))
                dirtySettings.Add("gyroSmoothingSave");

            GyroAccel.SetSmoothingTime(gyroSmoothingSave);
        }

        
        //gyroSettings.smoothingTime = gyroSmoothingSave;
        if (CustomSwitch())
        {
            Debug.Log("Smooth Save");
            controlFile.Save<float>("gyroSmoothingSave", gyroSmoothingSave);
            controlFile.Sync();
        }
    }

    /*
    public void flickToggleOnClick(bool flickSave)
    {
        flickSettings.enabled = flickSave;
        controlFile.Save<bool>("flickSave", flickSave);
    }
    */

    public void flickOnChange(int flickSave)
    {
        if(flickSettings.targetAxis != CF2Input.activeRig.axes.list[flickSave].name)
        {
            if (!dirtySettings.Contains("flickSave"))
                dirtySettings.Add("flickSave");

            flickSettings.targetAxis = CF2Input.activeRig.axes.list[flickSave].name;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("flickSave", flickSave);
            controlFile.Sync();
        }
    }

    public void flickSpeedInputFieldOnEnd(string flickSpeedSave)
    {
        if(flickSettings.minSwipeSpeed != float.Parse(flickSpeedSave))
        {
            if (!dirtySettings.Contains("flickSpeedSave"))
                dirtySettings.Add("flickSpeedSave");

            flickSettings.minSwipeSpeed = float.Parse(flickSpeedSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("flickSpeedSave", flickSpeedSave);
            controlFile.Sync();
        }
    }

    public void flickDistInputFieldOnEnd(string flickDistSave)
    {
        if(flickSettings.minSwipeThresh != float.Parse(flickDistSave))
        {
            if (!dirtySettings.Contains("flickDistSave"))
                dirtySettings.Add("flickDistSave");

            flickSettings.minSwipeThresh = float.Parse(flickDistSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("flickDistSave", flickDistSave);
            controlFile.Sync();
        }
    }

    public void flickTimeWindowInputFieldOnEnd(string flickTimeWindowSave)
    {
        if(flickSettings.timeWindow != float.Parse(flickTimeWindowSave))
        {
            if (!dirtySettings.Contains("flickTimeWindowSave"))
                dirtySettings.Add("flickTimeWindowSave");

            flickSettings.timeWindow = float.Parse(flickTimeWindowSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("flickTimeWindowSave", flickTimeWindowSave);
            controlFile.Sync();
        }
    }

    public void flickReqReleaseToggleOnClick(bool flickReqReleaseSave)
    {
        if(flickSettings.requireSwipeRelease != flickReqReleaseSave)
        {
            if (!dirtySettings.Contains("flickReqReleaseSave"))
                dirtySettings.Add("flickReqReleaseSave");

            flickSettings.requireSwipeRelease = flickReqReleaseSave;
        }

        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("flickReqReleaseSave", flickReqReleaseSave);
            controlFile.Sync();
        }
    }

    public void flickStartFromCenterToggleOnClick(bool flickStartFromCenterSave)
    {
        if(flickSettings.requireSwipeFromCenter != flickStartFromCenterSave)
        {
            if (!dirtySettings.Contains("flickStartFromCenterSave"))
                dirtySettings.Add("flickStartFromCenterSave");
            flickSettings.requireSwipeFromCenter = flickStartFromCenterSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("flickStartFromCenterSave", flickStartFromCenterSave);
            controlFile.Sync();
        }
    }

    public void rightTapOnChange(int rightTapSave)
    {
        if(rightSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].axisName != CF2Input.activeRig.axes.list[rightTapSave].name)
        {
            if (!dirtySettings.Contains("rightTapSave"))
                dirtySettings.Add("rightTapSave");

            rightSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].SetAxis(CF2Input.activeRig.axes.list[rightTapSave].name, true);
        }

        if (CustomSwitch())
        {
            controlFile.Save<int>("rightTapSave", rightTapSave);
            controlFile.Sync();
        }
    }

    public void rightDoubleTapOnChange(int rightDoubleTapSave)
    {
        if(rightDoubleTapSettings.targetAxis != CF2Input.activeRig.axes.list[rightDoubleTapSave].name)
        {
            if (!dirtySettings.Contains("rightDoubleTapSave"))
                dirtySettings.Add("rightDoubleTapSave");
            rightDoubleTapSettings.targetAxis = CF2Input.activeRig.axes.list[rightDoubleTapSave].name;
        }

        if (CustomSwitch())
        {
            controlFile.Save<int>("rightDoubleTapSave", rightDoubleTapSave);
            controlFile.Sync();
        }
    }

    public void right3dTouchOnChange(int right3dTouchSave)
    {
        if(rightPressureSettings.rangeConfigList[0].axisTarget != CF2Input.activeRig.axes.list[right3dTouchSave].name)
        {
            if (!dirtySettings.Contains("right3dTouchSave"))
                dirtySettings.Add("right3dTouchSave");
            rightPressureSettings.rangeConfigList[0].axisTarget = CF2Input.activeRig.axes.list[right3dTouchSave].name;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("right3dTouchSave", right3dTouchSave);
            controlFile.Sync();
        }
    }

    public void right3dTouchPressureSliderOnEnd(float right3dTouchPressureSave)
    {
        if(rightPressureSettings.rangeConfigList[0].min != right3dTouchPressureSave)
        {
            if (!dirtySettings.Contains("right3dTouchPressureSave"))
                dirtySettings.Add("right3dTouchPressureSave");
            rightPressureSettings.rangeConfigList[0].min = right3dTouchPressureSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<float>("right3dTouchPressureSave", right3dTouchPressureSave);
            controlFile.Sync();
        }
    }

    public void leftTapOnChange(int leftTapSave)
    {
        if(leftSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].axisName != CF2Input.activeRig.axes.list[leftTapSave].name)
        {
            if (!dirtySettings.Contains("leftTapSave"))
                dirtySettings.Add("leftTapSave");
            leftSettings.multiFingerConfigs[0].binding.tapBinding.axisList[0].SetAxis(CF2Input.activeRig.axes.list[leftTapSave].name, true);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("leftTapSave", leftTapSave);
            controlFile.Sync();
        }
    }

    public void leftDoubleTapOnChange(int leftDoubleTapSave)
    {
        if(leftDoubleTapSettings.targetAxis != CF2Input.activeRig.axes.list[leftDoubleTapSave].name)
        {
            if (!dirtySettings.Contains("leftDoubleTapSave"))
                dirtySettings.Add("leftDoubleTapSave");
            leftDoubleTapSettings.targetAxis = CF2Input.activeRig.axes.list[leftDoubleTapSave].name;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("leftDoubleTapSave", leftDoubleTapSave);
            controlFile.Sync();
        }
    }

    public void left3dTouchOnChange(int left3dTouchSave)
    {
        if(leftPressureSettings.rangeConfigList[0].axisTarget != CF2Input.activeRig.axes.list[left3dTouchSave].name)
        {
            if (!dirtySettings.Contains("left3dTouchSave"))
                dirtySettings.Add("left3dTouchSave");
            leftPressureSettings.rangeConfigList[0].axisTarget = CF2Input.activeRig.axes.list[left3dTouchSave].name;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("left3dTouchSave", left3dTouchSave);
            controlFile.Sync();
        }
    }

    public void left3dTouchPressureSliderOnEnd(float left3dTouchPressureSave)
    {
        if(leftPressureSettings.rangeConfigList[0].min != left3dTouchPressureSave)
        {
            if (!dirtySettings.Contains("left3dTouchPressureSave"))
                dirtySettings.Add("left3dTouchPressureSave");
            leftPressureSettings.rangeConfigList[0].min = left3dTouchPressureSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<float>("left3dTouchPressureSave", left3dTouchPressureSave);
            controlFile.Sync();
        }
    }

    public void leftHammerOnOnChange(int leftHammerOnSave)
    {
        if(leftHammerOnSettings.targetAxis != CF2Input.activeRig.axes.list[leftHammerOnSave].name)
        {
            if (!dirtySettings.Contains("leftHammerOnSave"))
                dirtySettings.Add("leftHammerOnSave");

            leftHammerOnSettings.targetAxis = CF2Input.activeRig.axes.list[leftHammerOnSave].name;
            if (leftHammerOnSettings.targetAxis == "None")
            {
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDelay = 0f;
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDuration = 0f;
                Debug.Log("Clear fade out delay and duration: " + leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDelay + " " + leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDuration);
            }
            else
            {
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDelay = leftHammerOnSettings.hammerOnGap - .1f;
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDuration = .1f;
                Debug.Log("Set fade out delay and duration to: " + (leftHammerOnSettings.hammerOnGap - .1f) + " and .1");
            }
        }
        

        if (CustomSwitch())
        {
            controlFile.Save<int>("leftHammerOnSave", leftHammerOnSave);
            controlFile.Sync();
        }
    }

    public void leftHammerOnMaxGapInputFieldOnEnd(string leftHammerOnMaxGapSave)
    {
        if(leftHammerOnSettings.hammerOnGap != float.Parse(leftHammerOnMaxGapSave))
        {
            if (!dirtySettings.Contains("leftHammerOnMaxGapSave"))
                dirtySettings.Add("leftHammerOnMaxGapSave");

            leftHammerOnSettings.hammerOnGap = float.Parse(leftHammerOnMaxGapSave);

            if (leftHammerOnSettings.targetAxis != "None")
            {
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDelay = leftHammerOnSettings.hammerOnGap - .1f;
                leftHammerOnSettings.GetComponentInChildren<DynamicTouchControl>().fadeOutDuration = .1f;
            }
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("leftHammerOnMaxGapSave", leftHammerOnMaxGapSave);
            controlFile.Sync();
        }
    }

    public void leftHammerOnMaxDistInputFieldOnEnd(string leftHammerOnMaxDistSave)
    {
        if(leftHammerOnSettings.hammerOnGap != float.Parse(leftHammerOnMaxDistSave))
        {
            if (!dirtySettings.Contains("leftHammerOnMaxDistSave"))
                dirtySettings.Add("leftHammerOnMaxDistSave");
            leftHammerOnSettings.hammerOnGap = float.Parse(leftHammerOnMaxDistSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("leftHammerOnMaxDistSave", leftHammerOnMaxDistSave);
            controlFile.Sync();
        }
    }

    public void rightHammerOnOnChange(int rightHammerOnSave)
    {
        if(rightHammerOnSettings.targetAxis != CF2Input.activeRig.axes.list[rightHammerOnSave].name)
        {
            if (!dirtySettings.Contains("rightHammerOnSave"))
                dirtySettings.Add("rightHammerOnSave");

            rightHammerOnSettings.targetAxis = CF2Input.activeRig.axes.list[rightHammerOnSave].name;
            if (rightHammerOnSettings.targetAxis == "None")
            {
                rightHammerOnSettings.transform.GetChild(0).gameObject.SetActive(false); //turn off hammer visualization
                Debug.Log("Turn off right side hammer on");
            }
            else
            {
                rightHammerOnSettings.transform.GetChild(0).gameObject.SetActive(true);
                Debug.Log("Turn on right side hammer on");
            }
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("rightHammerOnSave", rightHammerOnSave);
            controlFile.Sync();
        }
    }

    public void rightHammerOnMaxGapInputFieldOnEnd(string rightHammerOnMaxGapSave)
    {
        if(rightHammerOnSettings.hammerOnGap != float.Parse(rightHammerOnMaxGapSave))
        {
            if (!dirtySettings.Contains("rightHammerOnMaxGapSave"))
                dirtySettings.Add("rightHammerOnMaxGapSave");
            rightHammerOnSettings.hammerOnGap = float.Parse(rightHammerOnMaxGapSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("rightHammerOnMaxGapSave", rightHammerOnMaxGapSave);
            controlFile.Sync();
        }
    }

    public void rightHammerOnMaxDistInputFieldOnEnd(string rightHammerOnMaxDistSave)
    {
        if(rightHammerOnSettings.hammerOnGap != float.Parse(rightHammerOnMaxDistSave))
        {
            if (!dirtySettings.Contains("rightHammerOnMaxDistSave"))
                dirtySettings.Add("rightHammerOnMaxDistSave");
            rightHammerOnSettings.hammerOnGap = float.Parse(rightHammerOnMaxDistSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("rightHammerOnMaxDistSave", rightHammerOnMaxDistSave);
            controlFile.Sync();
        }
    }

    public void bottomRightSwipeUpOnChange(int bottomRightSwipeUpSave)
    {

        if(bottomRightSettings.multiFingerConfigs[0].binding.normalPressSwipeDirBinding.dirBindingU.axisList[0].axisName != CF2Input.activeRig.axes.list[bottomRightSwipeUpSave].name)
        {
            if (!dirtySettings.Contains("bottomRightSwipeUpSave"))
                dirtySettings.Add("bottomRightSwipeUpSave");
            bottomRightSettings.multiFingerConfigs[0].binding.normalPressSwipeDirBinding.dirBindingU.axisList[0].SetAxis(CF2Input.activeRig.axes.list[bottomRightSwipeUpSave].name, true);

            if (CF2Input.activeRig.axes.list[bottomRightSwipeUpSave].name == "None")
            {
                bottomRightSettings.gameObject.SetActive(false);
                Debug.Log("Turn off bottom right swipe");
            }
            else
            {
                bottomRightSettings.gameObject.SetActive(true);
                Debug.Log("Turn on bottom right swipe");
            }
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("bottomRightSwipeUpSave", bottomRightSwipeUpSave);
            controlFile.Sync();
        }
    }

    public void thrustOnChange(int thrustSave)
    {
        if(thrustSettings.targetAxis != CF2Input.activeRig.axes.list[thrustSave].name)
        {
            if (!dirtySettings.Contains("thrustSave"))
                dirtySettings.Add("thrustSave");
            thrustSettings.targetAxis = CF2Input.activeRig.axes.list[thrustSave].name;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<int>("thrustSave", thrustSave);
            controlFile.Sync();
        }
    }

    public void thrustThresholdInputFieldOnEnd(string thrustThresholdSave)
    {
        if(thrustSettings.accelerationThreshold != float.Parse(thrustThresholdSave))
        {
            if (!dirtySettings.Contains("thrustThresholdSave"))
                dirtySettings.Add("thrustThresholdSave");
            thrustSettings.accelerationThreshold = float.Parse(thrustThresholdSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("thrustThresholdSave", thrustThresholdSave);
            controlFile.Sync();
        }
    }

    public void dynamicJoystickToggleOnClick(bool dynamicJoystickSave)
    {
        if((joystickSettings.GetDynamicRegion() != joystickDynamicRegion && dynamicJoystickSave) || (joystickSettings.GetDynamicRegion() == joystickDynamicRegion && !dynamicJoystickSave))
        {
            if (!dirtySettings.Contains("dynamicJoystickSave"))
                dirtySettings.Add("dynamicJoystickSave");

            if (dynamicJoystickSave)
                joystickSettings.SetTargetDynamicRegion(joystickDynamicRegion);
            else
                joystickSettings.SetTargetDynamicRegion(null);
        }

        if (CustomSwitch())
        {
            controlFile.Save<bool>("dynamicJoystickSave", dynamicJoystickSave);
            controlFile.Sync();
        }
    }

    public void stickyJoystickToggleOnClick(bool stickyJoystickSave)
    {
        if(joystickSettings.stickyMode != stickyJoystickSave)
        {
            if (!dirtySettings.Contains("stickyJoystickSave"))
                dirtySettings.Add("stickyJoystickSave");
            joystickSettings.stickyMode = stickyJoystickSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("stickyJoystickSave", stickyJoystickSave);
            controlFile.Sync();
        }
    }

    public void minAnalogRangeSliderOnEnd(float minAnalogRangeSave)
    {
        if(joystickSettings.config.analogDeadZone != minAnalogRangeSave)
        {
            if (!dirtySettings.Contains("minAnalogRangeSave"))
                dirtySettings.Add("minAnalogRangeSave");
            joystickSettings.config.analogDeadZone = minAnalogRangeSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<float>("minAnalogRangeSave", minAnalogRangeSave);
            controlFile.Sync();
        }
    }

    public void maxAnalogRangeSliderOnEnd(float maxAnalogRangeSave)
    {
        if(joystickSettings.config.analogEndZone != maxAnalogRangeSave)
        {
            if (!dirtySettings.Contains("maxAnalogRangeSave"))
                dirtySettings.Add("maxAnalogRangeSave");
            joystickSettings.config.analogEndZone = maxAnalogRangeSave;
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<float>("maxAnalogRangeSave", maxAnalogRangeSave);
            controlFile.Sync();
        }
    }

    public void startValueAnalogRangeInputFieldOnEnd(string startValueAnalogRangeSave)
    {
        if(joystickSettings.config.analogRangeStartValue != float.Parse(startValueAnalogRangeSave))
        {
            if (!dirtySettings.Contains("startValueAnalogRangeSave"))
                dirtySettings.Add("startValueAnalogRangeSave");
            joystickSettings.config.analogRangeStartValue = float.Parse(startValueAnalogRangeSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<string>("startValueAnalogRangeSave", startValueAnalogRangeSave);
            controlFile.Sync();
        }
    }

    public void showBaseToggleOnClick(bool showBaseSave)
    {
        if(baseObj.activeSelf != showBaseSave)
        {
            if (!dirtySettings.Contains("showBaseSave"))
                dirtySettings.Add("showBaseSave");
            baseObj.SetActive(showBaseSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("showBaseSave", showBaseSave);
            controlFile.Sync();
        }
    }

    public void showHatToggleOnClick(bool showHatSave)
    {
        if(hatObj.activeSelf != showHatSave)
        {
            if (!dirtySettings.Contains("showHatSave"))
                dirtySettings.Add("showHatSave");
            hatObj.SetActive(showHatSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("showHatSave", showHatSave);
            controlFile.Sync();
        }
    }

    public void showDirectionToggleOnClick(bool showDirectionSave)
    {
        if(directionObj.activeSelf != showDirectionSave)
        {
            if (!dirtySettings.Contains("showDirectionSave"))
                dirtySettings.Add("showDirectionSave");
            directionObj.SetActive(showDirectionSave);
        }
        
        if (CustomSwitch())
        {
            controlFile.Save<bool>("showDirectionSave", showDirectionSave);
            controlFile.Sync();
        }
    }

}
