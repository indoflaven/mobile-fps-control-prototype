﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttachEnemy : MonoBehaviour {

    public Transform target;
    public Slider healthSlider;

	// Use this for initialization
	void Start () {
        healthSlider.maxValue = target.parent.GetComponent<vp_DamageHandler>().MaxHealth;

    }
	
	// Update is called once per frame
	void Update () {
        var wantedPos = Camera.main.WorldToScreenPoint(target.position);
        var currentHealth = target.parent.GetComponent<vp_DamageHandler>().CurrentHealth;
        healthSlider.value = (int)currentHealth;
        
        if (wantedPos.z < 0 || healthSlider.value == 0)
            wantedPos.x = 99999999;
        transform.position = wantedPos;
        
    }
}
