﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class Release : MonoBehaviour 
    {
	
	    public KeyCode targetKey				= KeyCode.None;
	    public string	targetAxis				= "";
	    public bool		targetAxisPositive	= true;
	
	    private DynamicTouchControl 
		    sourceControl;

	    private int axisId;


        // --------------------
        void OnEnable()
	    {
		    this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            
        }
	


	    // -----------------
	    void Update()
	    {

            if (this.sourceControl == null)
			    return;
		
		    var ts = this.sourceControl.TouchStateScreen;

            if (ts.JustReleasedRaw()) //when the control has just been released
            {
                triggerAxis();
            }


        }

        void triggerAxis()
        {
            if (!string.IsNullOrEmpty(this.targetAxis))
                this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);
        }


	
    }
}
