﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;


namespace ControlFreak2
{

public class FlickDetector : MonoBehaviour 
	{
	[Tooltip("Minimal swipe speed in cm/sec.")]
	public float	minSwipeSpeed			= 5.0f;

	[Tooltip("Minimal swipe threshold in cm.")]
	public float	minSwipeThresh			= 0.5f;

	[Tooltip("Sampling time window used for sustaining long fast swipes on jerky Android touch screens.")]
	[Range(0.1f, 1.0f)] 
	public float timeWindow = 0.3f;

	
	public KeyCode targetKey				= KeyCode.None;
	public string	targetAxis				= "";
	public bool		targetAxisPositive	= true;
	
	private DynamicTouchControl 
		sourceControl;
	
	private Vector2 startPos;
	private float timeElapsed;
	private int axisId;
	private bool swipeActive;
	
	// --------------------
	void OnEnable()
		{
		this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
		}
	


	// -----------------
	void Update()
		{


            


         if (this.sourceControl == null)
			return;


#if DRAW_DEBUG_GUI
		if (CF2Input.GetKeyDown(this.targetKey)) 
			{ this.debugFlickCount++; Debug.LogFormat("FLICK : {0} : fr:{1}", this.debugFlickCount,  Time.frameCount); }
#endif
		
		var ts = this.sourceControl.TouchStateScreen;
		
		if (ts.JustPressedRaw())
			{
			this.startPos = ts.GetStartPos();
			this.timeElapsed = 0;
			}
		
		if (ts.PressedRaw() || ts.JustReleasedRaw())
			{
			this.timeElapsed += Time.unscaledDeltaTime;
			
			Vector2 curPos = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosRaw());
			Vector3 delta = (curPos - this.startPos) * CFScreen.invDpcm;
			float dist = delta.magnitude;
			float speed = dist / this.timeElapsed;

#if DRAW_DEBUG_GUI
			this.debugDist = dist;
			this.debugSpeed = speed;
#endif

			if (!this.swipeActive && (dist > this.minSwipeThresh) && (speed > this.minSwipeSpeed))
				{
				this.swipeActive = true;
				this.timeElapsed = 0;
				this.startPos = curPos;
				}

			else if (this.timeElapsed > this.timeWindow) 
				{
				if (speed > this.minSwipeSpeed)
					{
					this.swipeActive = true;
					}
				else
					{
					this.swipeActive = false;
					}

				this.timeElapsed = 0;
				this.startPos = curPos;
				}


			if (this.swipeActive)
				{
				// Activate target input...

				if (!string.IsNullOrEmpty(this.targetAxis))	
					this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);

				if (this.targetKey != KeyCode.None)
					this.sourceControl.rig.SetKeyCode(this.targetKey);

				}

                Debug.DrawLine(this.startPos, curPos, Color.red, 5f);
            }

		else
			{
			this.swipeActive = false;
			this.timeElapsed = 0;
			}

            

        }




#if DRAW_DEBUG_GUI 

	private float 
		debugSpeed, 
		debugDist;
	private int 
		debugFlickCount;

	// ---------------------
	void OnGUI()
		{
		GUILayout.Box(string.Format("Flicks: {4}, ACTIVE: {0} \tspeed:{1} \t:dist:{2} \tt:{3}", this.swipeActive, 
			this.debugSpeed.ToString("0.00"), this.debugDist.ToString("0.00"), this.timeElapsed.ToString("0.000"),
			this.debugFlickCount)); 
		}

#endif


	
	}
}
