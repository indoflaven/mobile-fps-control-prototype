﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class HammerOn : MonoBehaviour 
    {

        public TouchButtonSpriteAnimator
            sourceControlAnimator;

        public KeyCode targetKey				= KeyCode.None;
	    public string	targetAxis				= "";
	    public bool		targetAxisPositive	= true;
	
	    private DynamicTouchControl 
		    sourceControl;



        private Vector2 startPos;
	    private int axisId;
	    private bool swipeActive;

        private float touchOnTimeElapsed;
        private float touchOffTimeElapsed;

        public float hammerOnGap;
        public float hammerOnMaxDist;

        public bool toggleMode = false;
        private bool toggle = false;

        private Vector2 prevTouchPos;
        private Vector2 curTouchPos;

        // --------------------
        void OnEnable()
	    {
		    this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            //if(toggleMode)
                //this.sourceControlAnimator = this.sourceControl.GetAnimatorList()[0].GetComponentInChildren<TouchButtonSpriteAnimator>();
            
        }
	


	    // -----------------
	    void Update()
	    {

            Vector3 delta = Vector3.zero;
            float dist = 0;

            if (this.sourceControl == null)
			    return;
		
		    var ts = this.sourceControl.TouchStateScreen;

            if (ts.JustPressedRaw()) //when the control has just been pressed
            {
                Debug.Log("Just Pressed");
                this.touchOnTimeElapsed = 0; //reset the timer that tracks how long the touch contacts the screens
                delta = (ts.GetCurPosRaw() - this.prevTouchPos) * CFScreen.invDpcm; //get the delta of this new touch and the previous touch
                dist = delta.magnitude; //find the distance between this touch and the prev touch
            }

            if (ts.JustReleasedRaw()) //when the control has just been released
            {
                this.touchOffTimeElapsed = 0; //reset the timer that tracks how long its been since the last touch
                this.prevTouchPos = ts.GetReleasedEndPos(); //track the completed touch as the previous touch for comparison purposes
            }

            if (!ts.PressedRaw()) //when the control is NOT being pressed
            {
                this.touchOffTimeElapsed += Time.unscaledDeltaTime; //track how long the control has not been touched
            }

            if (toggleMode && ts.JustPressedRaw() && (touchOffTimeElapsed > hammerOnGap || dist > hammerOnMaxDist)) //when the new touch fails to meet the conditions on a hammer on
            {
                toggle = false; //turn off the axis toggle
                sourceControlAnimator.SetStateColor(TouchButtonSpriteAnimator.ControlState.Pressed, Color.white);
                sourceControlAnimator.SetStateScale(TouchButtonSpriteAnimator.ControlState.Pressed, 1f);
            }

            if (ts.PressedRaw()) //when the control is being pressed
            {
                this.touchOnTimeElapsed += Time.unscaledDeltaTime; //track how long the control has been touched
            }

            if(ts.JustPressedRaw() && touchOffTimeElapsed <= hammerOnGap && dist <= hammerOnMaxDist) //when the control is touch check for a hammer on condition
            {
                Debug.Log("Hammeron Gap: " + touchOffTimeElapsed + "Hammeron Dist: " + dist);

                if (toggleMode) //if the toggle mode is True in the inspector settings
                { 
                    toggle = !toggle; //toggle on or off the axis depending on it's current state
                    if (toggle)
                    {
                        sourceControlAnimator.SetStateColor(TouchButtonSpriteAnimator.ControlState.Pressed, Color.red);
                        sourceControlAnimator.SetStateScale(TouchButtonSpriteAnimator.ControlState.Pressed, 1.2f);
                    }
                    else
                    {
                        sourceControlAnimator.SetStateColor(TouchButtonSpriteAnimator.ControlState.Pressed, Color.white);
                        sourceControlAnimator.SetStateScale(TouchButtonSpriteAnimator.ControlState.Pressed, 1f);
                    }
                        
                }
                else
                    triggerAxis(); //if the toggle mode is False don't worry about setting the toggle state, just trigger the axis
            }

            if (ts.PressedRaw()) //when the control is being pressed
            {
                if (toggle) //if the hammer on condition has been met previously trigger the axis
                    triggerAxis();
            }


            /*
            if (ts.JustPressedRaw())
            {
                this.startPos = ts.GetStartPos();
                this.timeElapsed = 0;
            }

            if (ts.PressedRaw() || ts.JustReleasedRaw())
            {
                this.timeElapsed += Time.unscaledDeltaTime;

                Vector2 curPos = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosRaw());
                Vector3 delta = (curPos - this.startPos) * CFScreen.invDpcm;
                float dist = delta.magnitude;
                float speed = dist / this.timeElapsed;



                if (!this.swipeActive && (dist > this.minSwipeThresh) && (speed > this.minSwipeSpeed))
                {
                    this.swipeActive = true;
                    this.timeElapsed = 0;
                    this.startPos = curPos;
                }

                else if (this.timeElapsed > this.timeWindow) 
                {
                    if (speed > this.minSwipeSpeed)
                    {
                        this.swipeActive = true;
                    }
                    else
                    {
                        this.swipeActive = false;
                    }

                    this.timeElapsed = 0;
                    this.startPos = curPos;
                }


                if (this.swipeActive)
                {
                    // Activate target input...

                    if (!string.IsNullOrEmpty(this.targetAxis))	
                        this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);

                    if (this.targetKey != KeyCode.None)
                        this.sourceControl.rig.SetKeyCode(this.targetKey);

                }

                Debug.DrawLine(this.startPos, curPos, Color.red, 5f);
            }

            else
            {
                this.swipeActive = false;
                this.timeElapsed = 0;
            }
            */


        }

        void triggerAxis()
        {
            if (!string.IsNullOrEmpty(this.targetAxis))
                this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);
        }


	
    }
}
