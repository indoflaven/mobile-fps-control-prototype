﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class TapRelease : MonoBehaviour 
    {
	
	    public string	tapAxis				= "";
	    public bool		tapAxisPositive	= true;

        public string releaseAxis = "";
        public bool releaseAxisPositive = true;

        private DynamicTouchControl 
		    sourceControl;

	    private int axisId;

        public float tapWindow;

        private float justPressedTime = 0;

        private bool axisToggle = false;

        private bool toggledOnPress = false;



        // --------------------
        void OnEnable()
	    {
		    this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            
        }
	


	    // -----------------
	    void Update()
	    {

            if (this.sourceControl == null)
			    return;
		
		    var ts = this.sourceControl.TouchStateScreen;

            if (ts.JustPressedRaw()) //when the control has just been released
            {
                justPressedTime = Time.time;

                if (!axisToggle)
                {
                    axisToggle = !axisToggle;
                    toggledOnPress = true;
                }
            }

            if (ts.JustReleasedRaw())
            {
                if ((Time.time - justPressedTime) <= tapWindow)
                {
                    if (!toggledOnPress)
                        axisToggle = !axisToggle;
                }
                else
                    triggerAxis(releaseAxis, releaseAxisPositive);

                toggledOnPress = false;
            }

            if (axisToggle)
                triggerAxis(tapAxis, tapAxisPositive);


        }

        void triggerAxis(string targetAxis, bool targetAxisPositive)
        {
            if (!string.IsNullOrEmpty(targetAxis))
                this.sourceControl.rig.SetAxisDigital(targetAxis, ref this.axisId, !targetAxisPositive);
        }


	
    }
}
