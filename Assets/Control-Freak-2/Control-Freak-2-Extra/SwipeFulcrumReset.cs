﻿
//#define DRAW_DEBUG_GUI
using System.Reflection;
using UnityEngine;
using ControlFreak2;

namespace ControlFreak2
{

public class SwipeFulcrumReset : MonoBehaviour 
{
    public Vector3 delta;

    [Tooltip("Minimal swipe speed in cm/sec.")]
	public float	minSwipeSpeed			= 5.0f;

	[Tooltip("Minimal swipe threshold in cm.")]
	public float	minSwipeThresh			= 0.5f;

	[Tooltip("Sampling time window used for sustaining long fast swipes on jerky Android touch screens.")]
	[Range(0.1f, 1.0f)] 
	public float timeWindow = 0.3f;

	private DynamicTouchControl 
		sourceControl;

    private TouchJoystick
        sourceControlJoystick;

    private Vector2 startPos;
    private Vector2 curPos;
    private Vector2 curPosSmooth;
    private float timeElapsed;
	private bool swipeActive;
	
	// --------------------
	void OnEnable()
	{
		 this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
         this.sourceControlJoystick = this.GetComponentInChildren<TouchJoystick>();
    }
	


	// -----------------
	void Update()
	{

        if (this.sourceControl == null)
		return;

		var ts = this.sourceControl.TouchStateScreen;

        if (ts.JustPressedRaw())
		{
			this.startPos = ts.GetStartPos();
			this.timeElapsed = 0;
		}


        if (ts.PressedRaw() || ts.JustReleasedRaw())
		{
			this.timeElapsed += Time.unscaledDeltaTime;
			curPos = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosRaw());
            curPosSmooth = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosSmooth());
            delta = (curPos - this.startPos) * CFScreen.invDpcm;
			float dist = delta.magnitude;
			float speed = dist / this.timeElapsed;

                
/*
            if  (requireSwipeFromCenter && this.sourceControlJoystick.GetDir() != CFUtils.VecToDir(delta, true)) //##Mike's Add - Trying to make it only count the swipe when starting from center of joystick
                    this.startPos = ts.GetCurPosRaw();
*/

            if (!this.swipeActive && (dist > this.minSwipeThresh) && (speed > this.minSwipeSpeed))
			{
                    this.swipeActive = true;
                    centerFulcrum();
                    this.timeElapsed = 0;
				    this.startPos = curPos;
			}
			else if (this.timeElapsed > this.timeWindow) 
			{
				if (speed > this.minSwipeSpeed)
				{
					    this.swipeActive = true;
                        centerFulcrum();

                }
				else
				{
					    this.swipeActive = false;
				}

				this.timeElapsed = 0;
				this.startPos = curPos;
			}


        }
		else
	    {
			this.swipeActive = false;
			this.timeElapsed = 0;
		}




    }

    void centerFulcrum()
    {
            Debug.Log("Center Fulcrum: " + this.sourceControlJoystick.GetDir() + " - " + CFUtils.VecToDir(delta, true));
            var swipeDir = CFUtils.VecToDir(delta, true);
            var joystickDir = this.sourceControlJoystick.GetDir();

            if (swipeDir == Dir.U || swipeDir == Dir.D || swipeDir == Dir.L || swipeDir == Dir.R)
            {
                if(joystickDir != swipeDir)
                {
                    if (swipeDir == Dir.U && (joystickDir == Dir.UR || joystickDir == Dir.UL))
                        return;
                    if (swipeDir == Dir.D && (joystickDir == Dir.DR || joystickDir == Dir.DL))
                        return;
                    if (swipeDir == Dir.L && (joystickDir == Dir.UL || joystickDir == Dir.DL))
                        return;
                    if (swipeDir == Dir.R && (joystickDir == Dir.UR || joystickDir == Dir.DR))
                        return;

                    this.sourceControl.SetWorldPos(startPos);

                }

            }
                
    }



}


}
