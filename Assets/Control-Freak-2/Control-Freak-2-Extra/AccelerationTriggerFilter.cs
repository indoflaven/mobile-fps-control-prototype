﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class AccelerationTriggerFilter : MonoBehaviour 
    {
	
	    public string	targetAxis				= "";
	    public bool		targetAxisPositive	    = true;
        //public float accelerationThreshold = -1.0f;

        private InputRig rig;

        private int axisId;

        private float accelerometerUpdateInterval = 1.0f / 60.0f;
        // The greater the value of LowPassKernelWidthInSeconds, the slower the filtered value will converge towards current input sample (and vice versa).
        private float lowPassKernelWidthInSeconds = 1.0f;
        // This next parameter is initialized to 2.0 per Apple's recommendation, or at least according to Brady! ;)
        public float accelerationThreshold = 2.0f;
 
        private float lowPassFilterFactor;
        private Vector3 lowPassValue = Vector3.zero;
        private Vector3 acceleration;
        private Vector3 deltaAcceleration;

        // --------------------
        void OnEnable()
	    {
            if (this.rig == null)
                this.rig = this.GetComponentInParent<InputRig>();

            lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
            accelerationThreshold *= accelerationThreshold;
            lowPassValue = Input.acceleration;
        }


// -----------------
        void Update()
        {

            acceleration = Input.acceleration;
            lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
            deltaAcceleration = acceleration - lowPassValue;
            if (deltaAcceleration.sqrMagnitude >= accelerationThreshold)
            {
                // Perform your "shaking actions" here, with suitable guards in the if check above, if necessary to not, to not fire again if they're already being performed.
                Debug.Log("Shake event: " + deltaAcceleration.z);
                triggerAxis();
            }
        }

        void triggerAxis()
        {
			if (!string.IsNullOrEmpty(this.targetAxis))
				rig.SetAxisDigital(this.targetAxis, ref this.axisId, this.targetAxisPositive);
        }


	
    }
}
