﻿
//#define DRAW_DEBUG_GUI

using System.Collections;
using UnityEngine;

namespace ControlFreak2
{

    public class ReturnToOriginOnRelease : MonoBehaviour 
    {
	
        public float waitTime = 0;
        public Vector2 offset = Vector2.zero;
        public GameObject followerButton;

        private DynamicTouchControl 
		    followerSourceControl;

        private DynamicTouchControl
            SourceControl;

        private Vector2 controlOrigin;
        private Vector2 _offset = Vector2.zero;
        private float _waitTime = 0;

        // --------------------
        void OnEnable()
	    {
		    this.SourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            this.followerSourceControl = followerButton.GetComponentInChildren<DynamicTouchControl>();
            controlOrigin = followerSourceControl.GetWorldPos();

        }
	


	    // -----------------
	    void Update()
	    {

            if (this.SourceControl == null)
			    return;
		
		    var ts = this.SourceControl.TouchStateScreen;
            var tsFollower = this.followerSourceControl.TouchStateScreen;

            if (ts.JustPressedRaw())
            {
                _offset = offset;
                _waitTime = waitTime;
            }



            if (tsFollower.JustReleasedRaw())
            {
                _waitTime = 0;
            }

            if (ts.PressedRaw())
            {
                if (tsFollower.PressedRaw())
                {
                    _offset = Vector2.zero;
                }
                Debug.Log(_offset);
                followerSourceControl.SetWorldPos(SourceControl.GetWorldPos() + (Vector3)_offset);
            }

            if (ts.JustReleasedRaw())
            {
                StartCoroutine(returnToOrigin(_waitTime));
            }

            /*
            if (ts.JustPressedRaw())
            {
                Debug.Log("Just pressed");
                _offset = offset;
                _waitTime = waitTime;
            }

            if (ts.JustReleasedRaw()) //when the control has just been released
            {
                Debug.Log("Just released");
                StartCoroutine(returnToOrigin(_waitTime, -_offset));
            }

            if (tsFollower.JustPressedRaw()) //when the control has just been pressed
            {
                Debug.Log("Button Just pressed");
                _offset = Vector2.zero;
                _waitTime = 0;
            }
            */


            



        }

        IEnumerator returnToOrigin(float wait)
        {
            Debug.Log(wait + " " + controlOrigin);
            yield return new WaitForSeconds(wait);


            followerSourceControl.SetWorldPos(controlOrigin);
        }

        /*
        IEnumerator returnToOrigin(float wait, Vector2 theOffset)
        {
            Debug.Log(wait + " " + controlOrigin);
            yield return new WaitForSeconds(wait);

            
            SourceControl.SetWorldPos(controlOrigin + theOffset, true);
        }
        */



    }
}
