﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class Tap : MonoBehaviour 
    {

        public controlType controlStyle = controlType.DynamicTouchControl;
        public string	tapAxis				= "";
	    public bool		tapAxisPositive	= true;

        private DynamicTouchControl
            sourceControlDynamicTouchControl;

        private SuperTouchZone
            sourceControlSuperTouchZone;

        private int axisId;

        public float tapWindow;

        public bool toggleMode = false;

        private float justPressedTime = 0;

        private bool axisToggle = false;

        public enum controlType { DynamicTouchControl, SuperTouchZone };




        // --------------------
        void OnEnable()
	    {

            if (controlStyle == controlType.DynamicTouchControl)
                this.sourceControlDynamicTouchControl = this.GetComponentInChildren<DynamicTouchControl>();

            if (controlStyle == controlType.SuperTouchZone)
                this.sourceControlSuperTouchZone = this.GetComponentInChildren<SuperTouchZone>();

        }
	


	    // -----------------
	    void Update()
	    {



            if (controlStyle == controlType.DynamicTouchControl)
            {

                if (this.sourceControlDynamicTouchControl == null)
                    return;

                var ts = this.sourceControlDynamicTouchControl.TouchStateScreen;

                if (ts.JustPressedRaw()) //when the control has just been released
                {
                    justPressedTime = Time.time;
                }

                if (ts.JustReleasedRaw())
                {
                    if ((Time.time - justPressedTime) <= tapWindow)
                    {
                        if (toggleMode)
                            axisToggle = !axisToggle;
                        else
                            triggerAxis(tapAxis, tapAxisPositive);
                    }
                }

                if (axisToggle)
                    triggerAxis(tapAxis, tapAxisPositive);

            }

            if (controlStyle == controlType.SuperTouchZone)
            {

                if (this.sourceControlSuperTouchZone == null)
                    return;

                if (sourceControlSuperTouchZone.JustTapped(1, 1)) //when the control has just been released
                {
                    if (toggleMode)
                        axisToggle = !axisToggle;
                    else
                        triggerAxis(tapAxis, tapAxisPositive);
                }


                if (axisToggle)
                    triggerAxis(tapAxis, tapAxisPositive);

            }

        }

        void triggerAxis(string targetAxis, bool targetAxisPositive)
        {
            if (!string.IsNullOrEmpty(targetAxis))
            {
                if (controlStyle == controlType.DynamicTouchControl)
                    this.sourceControlDynamicTouchControl.rig.SetAxisDigital(targetAxis, ref this.axisId, !targetAxisPositive);
                if (controlStyle == controlType.SuperTouchZone)
                    this.sourceControlSuperTouchZone.rig.SetAxisDigital(targetAxis, ref this.axisId, !targetAxisPositive);
            }
                
        }


	
    }
}
