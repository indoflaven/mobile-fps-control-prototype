﻿
//#define DRAW_DEBUG_GUI

using System.Collections;
using UnityEngine;

namespace ControlFreak2
{

    public class StickyButton : MonoBehaviour 
    {
	
        public float waitTime = 0;
        public Vector2 offset = Vector2.zero;

        private DynamicTouchControl
            SourceControl;

        private Vector2 controlOrigin;
        private Vector2 _offset = Vector2.zero;
        private float _waitTime = 0;

        // --------------------
        void OnEnable()
	    {
		    this.SourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            controlOrigin = SourceControl.GetWorldPos();

        }
	


	    // -----------------
	    void Update()
	    {

            if (this.SourceControl == null)
			    return;
		
		    var ts = this.SourceControl.TouchStateScreen;

            if (ts.PressedRaw())
            {
                SourceControl.SetWorldPos(ts.GetCurPosRaw() + _offset);
            }
            

            /*

            if (ts.JustPressedRaw())
            {
                Debug.Log("Just pressed");
                _offset = offset;
                _waitTime = waitTime;
            }

            if (tsFollower.JustPressedRaw()) //when the control has just been pressed
            {
                Debug.Log("Button Just pressed");
                _offset = Vector2.zero;
                _waitTime = 0;
            }

            if (ts.JustReleasedRaw()) //when the control has just been released
            {
                Debug.Log("Just released");
                StartCoroutine(returnToOrigin(_waitTime, -_offset));
            }
            */


        }

        IEnumerator returnToOrigin(float wait, Vector2 theOffset)
        {
            Debug.Log(wait + " " + controlOrigin + theOffset);
            yield return new WaitForSeconds(wait);

            
            SourceControl.SetWorldPos(controlOrigin + theOffset);
        }




    }
}
