﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class DoubleTapAndHold : MonoBehaviour 
    {
	
	    public KeyCode targetKey				= KeyCode.None;
	    public string	targetAxis				= "";
	    public bool		targetAxisPositive	= true;
	
	    private SuperTouchZone 
		    sourceControl;

	    private int axisId;

        private float justTappedTime = 0;

        private bool triggered = false;

        // --------------------
        void OnEnable()
	    {
		    this.sourceControl = this.GetComponentInChildren<SuperTouchZone>();
            
        }
	


	    // -----------------
	    void Update()
	    {

            if (this.sourceControl == null)
			    return;
		
		    //var ts = this.sourceControl.TouchStateScreen;
            //sourceControl.

            if (sourceControl.JustTapped(1,1)) //when the control has just been released
            {
                justTappedTime = Time.time;
                Debug.Log("Just Tapped");
            }

            if (sourceControl.JustPressedRaw(1) && Time.time <= (justTappedTime + sourceControl.GetThresholds().multiTapMaxTimeGap))
            {
                Debug.Log("Double Tap and Hold");
                triggered = true;
                
            }

            if (sourceControl.JustReleasedRaw(1) && triggered)
                triggered = false;

            if(triggered)
                triggerAxis();



        }

        void triggerAxis()
        {
            if (!string.IsNullOrEmpty(this.targetAxis))
                this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);
        }


	
    }
}
