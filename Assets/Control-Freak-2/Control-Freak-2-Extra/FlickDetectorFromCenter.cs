﻿
//#define DRAW_DEBUG_GUI
using System.Reflection;
using UnityEngine;
using ControlFreak2;

namespace ControlFreak2
{

public class FlickDetectorFromCenter : MonoBehaviour 
	{
        public Vector3 delta;
        public GameObject thumbTrail;
        private ParticleSystem thumbTrailPS;
        public GameObject thumbTrigger;
        private ParticleSystem thumbTriggerPS;
        public Camera uiCamera;
        public bool requireSwipeRelease;
        public bool requireSwipeFromCenter;
        public float gracePeriod;
        private float gracePeriodStart=-99;


    [Tooltip("Minimal swipe speed in cm/sec.")]
	public float	minSwipeSpeed			= 5.0f;

	[Tooltip("Minimal swipe threshold in cm.")]
	public float	minSwipeThresh			= 0.5f;

	[Tooltip("Sampling time window used for sustaining long fast swipes on jerky Android touch screens.")]
	[Range(0.1f, 1.0f)] 
	public float timeWindow = 0.3f;



    public KeyCode targetKey				= KeyCode.None;
	public string	targetAxis				= "";
	public bool		targetAxisPositive	= true;
	
	private DynamicTouchControl 
		sourceControl;

    private TouchJoystick
        sourceControlJoystick;

    private Vector2 startPos;
    private Vector2 curPos;
    private Vector2 curPosSmooth;
    private float timeElapsed;
	private int axisId;
	private bool swipeActive;
	
	// --------------------
	void OnEnable()
		{
		    this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            this.sourceControlJoystick = this.GetComponentInChildren<TouchJoystick>();

            thumbTrailPS = thumbTrail.GetComponent<ParticleSystem>();
            thumbTriggerPS = thumbTrigger.GetComponent<ParticleSystem>();
        }
	


	// -----------------
	void Update()
		{

            
            


         if (this.sourceControl == null)
			return;


#if DRAW_DEBUG_GUI
		if (CF2Input.GetKeyDown(this.targetKey)) 
			{ this.debugFlickCount++; Debug.LogFormat("FLICK : {0} : fr:{1}", this.debugFlickCount,  Time.frameCount); }
#endif
		
		    var ts = this.sourceControl.TouchStateScreen;



            if (ts.JustPressedRaw())
			{
			this.startPos = ts.GetStartPos();
			this.timeElapsed = 0;
			}

            bool JustReleasedBool = (requireSwipeRelease) ? ts.JustReleasedRaw() : true;



        if (ts.PressedRaw() || ts.JustReleasedRaw())
			{
			this.timeElapsed += Time.unscaledDeltaTime;
			curPos = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosRaw());
            curPosSmooth = (ts.JustReleasedRaw() ? ts.GetReleasedEndPos() : ts.GetCurPosSmooth());
            delta = (curPos - this.startPos) * CFScreen.invDpcm;
			float dist = delta.magnitude;
			float speed = dist / this.timeElapsed;

                

            if  (requireSwipeFromCenter && this.sourceControlJoystick.GetDir() != CFUtils.VecToDir(delta, true)) //##Mike's Add - Trying to make it only count the swipe when starting from center of joystick
                    this.startPos = ts.GetCurPosRaw();

#if DRAW_DEBUG_GUI
			this.debugDist = dist;
			this.debugSpeed = speed;
#endif


                if (!this.swipeActive && (dist > this.minSwipeThresh) && (speed > this.minSwipeSpeed))
				{
                    
                    this.swipeActive = true;
				    this.timeElapsed = 0;
				    this.startPos = curPos;
				}

			else if (this.timeElapsed > this.timeWindow) 
				{
				if (speed > this.minSwipeSpeed)
					{
					this.swipeActive = true;
					}
				else
					{
					this.swipeActive = false;
					}

				this.timeElapsed = 0;
				this.startPos = curPos;
				}





                if (JustReleasedBool && (this.swipeActive || inGracePeriod()))
				{
                // Activate target input...
                Debug.Log("curPos: " + curPos + " startPos: " + this.startPos + " delta: " + delta + " dist: " + dist + " speed: " + speed);
                    //Debug.Log(this.sourceControlJoystick.GetDir() + " - " + CFUtils.VecToDir(delta, true));

                    thumbTriggerPS.Play();
                    gracePeriodStart = -1;

                    if (!string.IsNullOrEmpty(this.targetAxis))	
					    this.sourceControl.rig.SetAxisDigital(this.targetAxis, ref this.axisId, !this.targetAxisPositive);

				    if (this.targetKey != KeyCode.None)
					    this.sourceControl.rig.SetKeyCode(this.targetKey);



                }



                //Drawing.DrawLine(this.startPos, curPos, Color.red, 1f, true);
                //DrawLine(this.startPos, curPos, Color.red, 5f);
            }

		else
			{
			this.swipeActive = false;
			this.timeElapsed = 0;
			}

            var thumbFXpos = uiCamera.ScreenToWorldPoint(new Vector3(curPosSmooth.x, curPosSmooth.y, thumbTrail.transform.position.z));
            thumbTrail.transform.position = thumbFXpos;
            thumbTrigger.transform.position = thumbFXpos;

            if (this.swipeActive)
            {
                gracePeriodStart = Time.time;
            }

            if (this.swipeActive && thumbTrailPS.isStopped)
            {
                thumbTrailPS.Play();
            }
            if (!this.swipeActive && !inGracePeriod())
            {
                thumbTrailPS.Stop();
            }


        }


        bool inGracePeriod()
        {
            if (Time.time <= gracePeriodStart + gracePeriod)
                return true;
            else
                return false;
        }

#if DRAW_DEBUG_GUI

	private float 
		debugSpeed, 
		debugDist;
	private int 
		debugFlickCount;

	// ---------------------
	void OnGUI()
		{
		GUILayout.Box(string.Format("Flicks: {4}, ACTIVE: {0} \tspeed:{1} \t:dist:{2} \tt:{3}", this.swipeActive, 
			this.debugSpeed.ToString("0.00"), this.debugDist.ToString("0.00"), this.timeElapsed.ToString("0.000"),
			this.debugFlickCount)); 
		}

#endif



    }


}
