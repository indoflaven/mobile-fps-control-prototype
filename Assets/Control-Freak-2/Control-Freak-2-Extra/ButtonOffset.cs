﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ControlFreak2
{


    public class ButtonOffset : MonoBehaviour
    {

        public GameObject followButton;
        public Vector2 offset;

        private DynamicTouchControl
            sourceControl;

        private DynamicTouchControl
            followingSourceControl;

        private Vector2 _offset;

        // --------------------
        void OnEnable()
        {
            this.sourceControl = this.GetComponentInChildren<DynamicTouchControl>();
            this.followingSourceControl = followButton.GetComponentInChildren<DynamicTouchControl>();
            _offset = offset;
        }

        // -----------------
        void Update()
        {
            var ts = this.sourceControl.TouchStateScreen;
            if (ts.JustPressedRaw()) //when the control has just been pressed
            {
                _offset = Vector2.zero;
            }
            if (ts.JustReleasedRaw()) //when the control has just been released
            {
                _offset = offset;
            }

            sourceControl.SetWorldPos(followingSourceControl.GetWorldPos() + (Vector3)_offset);
        }




    }
}