﻿
//#define DRAW_DEBUG_GUI

using UnityEngine;

namespace ControlFreak2
{

    public class AccelerationTrigger : MonoBehaviour 
    {
	
	    public string	targetAxis				= "";
	    public bool		targetAxisPositive	    = true;
        public float accelerationThreshold = -1.0f;

        private InputRig rig;

        private int axisId;


        // --------------------
        void OnEnable()
	    {
            if (this.rig == null)
                this.rig = this.GetComponentInParent<InputRig>();
        }
	


	    // -----------------
	    void Update()
	    {

            //Debug.Log(Input.acceleration);


            if((accelerationThreshold < 0 && Input.acceleration.z <= accelerationThreshold) || (accelerationThreshold > 0 && Input.acceleration.z >= accelerationThreshold))
            {
                Debug.Log(Input.acceleration.z);
                triggerAxis();
            }

        }

        void triggerAxis()
        {
			if (!string.IsNullOrEmpty(this.targetAxis))
				rig.SetAxisDigital(this.targetAxis, ref this.axisId, this.targetAxisPositive);
        }


	
    }
}
