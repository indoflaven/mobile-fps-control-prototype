﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLive : MonoBehaviour {

    public float Damage;
    protected static vp_DamageHandler m_TargetDHandler = null;

    // Use this for initialization
    void Start () {
        GameObject.Destroy(this.gameObject, 15);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnCollisionEnter(Collision collision)
    {


        m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(collision.collider);
        if (m_TargetDHandler != null)
        {
            DoUFPSDamage();
            Destroy(GetComponent<Rigidbody>());
            //GetComponent<BoxCollider>().enabled = false;

            transform.position = collision.contacts[0].point;
            transform.parent = collision.transform;
        }
        GameObject.Destroy(this.gameObject, 5);

    }


    protected virtual void DoUFPSDamage()
{

    m_TargetDHandler.Damage(new vp_DamageInfo(Damage, this.transform));

}

}
