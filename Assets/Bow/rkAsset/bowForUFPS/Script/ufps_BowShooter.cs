﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
///
/// </summary>
public partial class ufps_BowShooter : vp_Component//MonoBehaviour
{
    public GameObject _prefabArrow;
    public float _shotForce = 25;
    public float _animSpeed = 1;
    public Vector3 positionOffset=Vector3.zero;
    Animation _anim;
    Transform _refArrow;
    vp_FPWeapon m_Weapon;
    vp_PlayerInventory _invent;
    //
	protected AudioSource _Audio = null;
	public AudioClip _SoundShot = null;
	public AudioClip _SoundReload = null;
    float _pressTime = 0;
    bool _mouseDown = false;
    const string STR_ANIM_bow_in2 = "bow_in2";
    const string STR_ANIM_bow_setArrow = "bow_setArrow";
    const string STR_ANIM_bow_shot = "bow_shot";
    bool _inputAttackOld = false;
    bool _inputAttack = false;

    bool Setup()
    {
        if (_anim != null)
            return true;
        Transform bow = transform;//.FindChild("Bow(Clone)");
        if (bow == null)
            return false;
        //
        _Audio = bow.parent.GetComponent<AudioSource>();
        _anim = bow.GetComponent<Animation>();
        _refArrow = bow.Find("Bow/WeaponMainLocator");
        m_Weapon = transform.parent.GetComponent<vp_FPWeapon>();
        _invent = transform.parent.parent.GetComponent<vp_PlayerInventory>();
        if(_invent==null)
            _invent = transform.parent.parent.parent.parent.GetComponent<vp_PlayerInventory>();

        ReloadArrow();
        return true;
    }

	protected override void Update()
    {

        if (Setup() == false) return;
        if (IsUfpsAttackMode() == false) return;
        if (IsReloading()) return;
        //
        _inputAttackOld = _inputAttack;
        _inputAttack = Player.Attack.Active;

        //
		if(_inputAttack && _inputAttackOld==false )//if(Input.GetMouseButtonDown(0))
        {
            if (IsArrow() == false)
            {
                ReloadArrow();
                return;
            }else
            {
                _anim.Play(STR_ANIM_bow_in2);
                _pressTime = Time.time;
                _mouseDown = true;
            }
        }
        else if(_inputAttack==false && _inputAttackOld)//else if(Input.GetMouseButtonUp(0))
        {
            if (_mouseDown)
            {
                //Debug.Log("Shoot Arrow");
                ShotArrow();
            }

             _mouseDown = false;
        }
        if(Player.Reload.Active) //if (Input.GetKeyUp(KeyCode.R))
        {
            if (IsArrow() == false)
                ReloadArrow();
        }
        //if (Input.GetKeyUp(KeyCode.O))
        //    RemoveAllArrow();
    }
    void ReloadArrow()
    {
        if(_invent.GetAmmoInCurrentWeapon()>0)
        {
            vp_UnitBankInstance bank = _invent.GetItem(_invent.CurrentWeaponIdentifier.GetItemType()) as vp_UnitBankInstance;
            if(_invent.TryRemoveUnits((vp_UnitType)bank.UnitType,1))
            {
                _anim[STR_ANIM_bow_setArrow].speed = _animSpeed;
                _anim.Play(STR_ANIM_bow_setArrow);                
                PlaySound(_SoundReload);
            }
        }
        
    }
    bool IsReloading()
    {
        return _anim.IsPlaying(STR_ANIM_bow_setArrow);
    }
    void ShotArrow()
    {
        if (IsArrow())
        {
            _anim.Play(STR_ANIM_bow_shot);
            PlaySound(_SoundShot);
        }
    }
    void PlaySound(AudioClip clip)
    {
		if (_Audio != null || clip!=null)
		{
			_Audio.pitch = Time.timeScale;
			_Audio.PlayOneShot(clip);
		}
    }
    bool IsArrow()
    {
        return _refArrow.gameObject.activeSelf;
    }
    public void RemoveAllArrow()
    {
        if (Setup() == false) return;

        _refArrow.gameObject.SetActive(false);
        vp_UnitBankInstance bank = _invent.GetItem(_invent.CurrentWeaponIdentifier.GetItemType()) as vp_UnitBankInstance;
        bank.TryRemoveUnits(_invent.GetAmmoInCurrentWeapon());
    }
    void AnimEvent_OnReloadArrowed()
    {

    }
    void AnimEvent_OnShotAnimEnd()
    {
        Shot_Arrow();
        ReloadArrow();
    }
    void Shot_Arrow()
    {
        //
        float pressPower = Mathf.Clamp(Time.time - _pressTime,0.3f, 1);
        //
        GameObject arrow = GameObject.Instantiate<GameObject>(_prefabArrow);
        arrow.transform.localScale = _refArrow.lossyScale;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2,  Screen.height / 2, 0));
        arrow.transform.position = ray.origin+ray.direction;
        
        arrow.transform.LookAt(arrow.transform.position + ray.direction);
        arrow.transform.Translate(positionOffset);
        arrow.GetComponent<Rigidbody>().velocity = ray.direction * _shotForce*pressPower;
    }
}
public partial class ufps_BowShooter : vp_Component//MonoBehaviour
{
    bool IsUfpsAttackMode()
    {
		//if (!Player.Attack.Active)
			//return false;

		if (Player.SetWeapon.Active)
			return false;

		if (m_Weapon == null)
			return false;
		if (!m_Weapon.Wielded)
			return false;
        return true;
    }
    vp_FPPlayerEventHandler m_Player = null;
    vp_FPPlayerEventHandler Player
    {
        get
        {
            if (m_Player == null)
            {
                if (EventHandler != null)
                    m_Player = (vp_FPPlayerEventHandler)EventHandler;
            }
            return m_Player;
        }
    }


}