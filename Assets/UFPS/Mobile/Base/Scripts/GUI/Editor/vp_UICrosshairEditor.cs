/////////////////////////////////////////////////////////////////////////////////
//
//	vp_UICrosshairEditor.cs
//	© Opsive. All Rights Reserved.
//	https://twitter.com/Opsive
//	http://www.opsive.com
//
//	description:	custom inspector for the vp_UICrosshair class
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[CustomEditor(typeof(vp_UICrosshair))]
public class vp_UICrosshairEditor : vp_UIControlEditor
{

	protected vp_UICrosshair m_Target = null;
	
	protected override void OnEnable()
	{
	
		base.OnEnable();
		
		m_Target = (vp_UICrosshair)target;
	
	}
	

	/// <summary>
	/// 
	/// </summary>
	public override void OnInspectorGUI()
	{

		Undo.RecordObject(target, "HUD Control Snapshot");

		GUI.color = Color.white;
		
		GUILayout.Space(10);

        //m_Target.gyroAccelScript = (GyroAccel)EditorGUILayout.ObjectField("Gyro Accel Script", m_Target.gyroAccelScript, typeof(GyroAccel), true);

        m_Target.menuScript = (Menu)EditorGUILayout.ObjectField("Menu Script", m_Target.menuScript, typeof(Menu), true);

        m_Target.EnemyCrosshair = (Texture)EditorGUILayout.ObjectField("Enemy Crosshair", m_Target.EnemyCrosshair, typeof(Texture), true);
		m_Target.EnemyMask = vp_EditorGUIUtility.LayerMaskField("Enemy Mask", m_Target.EnemyMask, true);
		m_Target.EnemyColor = EditorGUILayout.ColorField("Enemy Color", m_Target.EnemyColor);
		m_Target.ColorChangeDuration = EditorGUILayout.FloatField("Color Change Duration", m_Target.ColorChangeDuration);
		m_Target.InteractIconSize = EditorGUILayout.Slider("Interact Icon Size", m_Target.InteractIconSize, 0, 10);
		GUILayout.BeginHorizontal();
		m_Target.ScaleOnRun = EditorGUILayout.Toggle("Scale on Run", m_Target.ScaleOnRun);
		if(m_Target.ScaleOnRun)
		{
			GUILayout.Space(10);
			GUILayout.Label("Multiplier");
			m_Target.ScaleMultiplier = EditorGUILayout.Slider(m_Target.ScaleMultiplier, 0, 10);
		}
		GUILayout.EndHorizontal();
		m_Target.AssistedTargetingFoldout = EditorGUILayout.Foldout(m_Target.AssistedTargetingFoldout, "Assisted Targeting");
		if(m_Target.AssistedTargetingFoldout)
		{
			EditorGUI.indentLevel++;
			m_Target.AssistedTargetingEnabled = EditorGUILayout.Toggle("Enable", m_Target.AssistedTargetingEnabled);
            m_Target.hammerOnTarget = EditorGUILayout.Toggle("Hammer On", m_Target.hammerOnTarget);
            m_Target.AssistedTargetingRequireLineOfSight = EditorGUILayout.Toggle("Require Line of Sight", m_Target.AssistedTargetingRequireLineOfSight);
            if (m_Target.AssistedTargetingRequireLineOfSight)
            {
                EditorGUI.indentLevel++;
                m_Target.AssistedTargetingLOSTimeout = EditorGUILayout.FloatField("LOS Break Timeout", m_Target.AssistedTargetingLOSTimeout);
                EditorGUI.indentLevel--;
            }

            

            m_Target.AssistedTargetingLockCrosshair = EditorGUILayout.Toggle("Use the Targeting Crosshair", m_Target.AssistedTargetingLockCrosshair);
            m_Target.AssistedTargetingCrosshairSmoothing = EditorGUILayout.Toggle("Smooth the Targeting Crosshair movement", m_Target.AssistedTargetingCrosshairSmoothing);
            if (m_Target.AssistedTargetingCrosshairSmoothing)
            {
                EditorGUI.indentLevel++;
                m_Target.AssistedTargetingSpeed = EditorGUILayout.Slider("Crosshair Tracking Speed", m_Target.AssistedTargetingSpeed, 0, 100);
                EditorGUI.indentLevel--;
            }
                
            m_Target.AssistedTargetingCameraSpeed = EditorGUILayout.Slider("Camera Tracking Speed", m_Target.AssistedTargetingCameraSpeed, 0, 100);
            m_Target.AssistedTargetingRadius = EditorGUILayout.Slider("Lock On Radius", m_Target.AssistedTargetingRadius, 0, 10);
			m_Target.AssistedTargetingTrackingRadius = EditorGUILayout.Slider("Tracking Radius", m_Target.AssistedTargetingTrackingRadius, 0, 10);
            m_Target.AssistedTargetingDistance = EditorGUILayout.FloatField("Max Distance", m_Target.AssistedTargetingDistance);
            EditorGUI.indentLevel--;
		}
		
		GUILayout.Space(10);

        m_Target.AutoFireFoldout = EditorGUILayout.Foldout(m_Target.AutoFireFoldout, "Auto Fire");
        if (m_Target.AutoFireFoldout)
        {
            m_Target.AutoFireEnabled = EditorGUILayout.Toggle("Enable", m_Target.AutoFireEnabled);
            m_Target.AutoFireRequireLineOfSight = EditorGUILayout.Toggle("Require LOS", m_Target.AutoFireRequireLineOfSight);
            if (m_Target.AutoFireRequireLineOfSight)
            {
                EditorGUI.indentLevel++;
                m_Target.AutoFireLOSTimeout = EditorGUILayout.FloatField("LOS Break Timeout", m_Target.AutoFireLOSTimeout);
                EditorGUI.indentLevel--;
            }

            
            m_Target.AutoFireStartRadius = EditorGUILayout.Slider("Start Fire Radius", m_Target.AutoFireStartRadius, 0, 10);
            m_Target.AutoFireContinueRadius = EditorGUILayout.Slider("Continue Fire Radius", m_Target.AutoFireContinueRadius, 0, 10);
            m_Target.AutoFireDistance = EditorGUILayout.FloatField("Max Distance", m_Target.AutoFireDistance);
            m_Target.AutoFireTargetAcquisitionTime = EditorGUILayout.FloatField("Acquisition Time", m_Target.AutoFireTargetAcquisitionTime);
            m_Target.AutoFireCrosshair = (Image)EditorGUILayout.ObjectField("Crosshair", m_Target.AutoFireCrosshair, typeof(Image), true);


        }
        // update
        if (GUI.changed)
		{

			EditorUtility.SetDirty(target);

		}

	}
	

}

