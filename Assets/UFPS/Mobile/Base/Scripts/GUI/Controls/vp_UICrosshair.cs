﻿/////////////////////////////////////////////////////////////////////////////////
//
//	vp_UICrosshair.cs
//	© Opsive. All Rights Reserved.
//	https://twitter.com/Opsive
//	http://www.opsive.com
//
//	description:	this script manages a textured crosshair on a mesh
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using MSP_Input;
using ControlFreak2;


public class vp_UICrosshair : vp_UIControl
{
    public Menu menuScript; //Mike's Add - ref the menu script
    public bool hammerOnTarget = false;
    protected string previousHammerOnAxis;

    public Image AutoFireCrosshair;


    public bool AutoFiring = false;
    public bool AutoFireEnabled = false;
    public float AutoFireDistance = Mathf.Infinity;
    public float AutoFireTargetAcquisitionTime = 0f;
    protected float AutoFireTargetAcquisitionTimeStart = -1;
    public bool AutoFireRequireLineOfSight = true; //is line of sight required to aquire and keep target
    public float AutoFireLOSTimeout = 0;       //How long after line of sight is lost should assisted targeting stop
    protected float AutoFireLastTrackedTime = 0;//Has the LOS Timeout expired yet?
    public float AutoFireStartRadius = 1f;          // the radius of the raycast that will be used to detect enemies
    public float AutoFireContinueRadius = 2f;  // when an enemy is being tracked, this is the radius threshold to stay within to keep tracking
    protected float startingAutoFireContinueRadius;
    public bool AutoFireFoldout = false;		// for editor use

    public Color EnemyColor = Color.red;				// color of crosshair when over an enemy
	public float ColorChangeDuration = .25f;			// speed at which the color is changed
	public LayerMask EnemyMask = 1 << vp_Layer.Enemy;	// enemy layers
	public float InteractIconSize = 2;					// size that this gameobject will change to when interaction is available
	public Texture EnemyCrosshair = null;				// textre that will be changed to when crosshair is over an enemy
	public bool ScaleOnRun = true;						// should the crosshair scale when running is active
	public float ScaleMultiplier = 1.25f;				// multiplies by the original crosshair size for ScaleOnRun
	public bool AssistedTargetingEnabled = true;               // should assisted targeting be allowed
    public bool AssistedTargetingRequireLineOfSight = true; //is line of sight required to aquire and keep target
    public float AssistedTargetingLOSTimeout = 0;       //How long after line of sight is lost should assisted targeting stop
    protected float AssistedTargetingLastTrackedTime = 0;//Has the LOS Timeout expired yet?
    public bool AssistedTargetingLockCrosshair = true;  //Show the lock on crosshair?
    public bool AssistedTargetingCrosshairSmoothing = false;//Use Assisted Targeting Speed?
    public float AssistedTargetingSpeed = 20;			// speed of assisted targeting interpolation of the crosshair
    public float AssistedTargetingCameraSpeed = 3;      // speed of assisted targeting interpolation of the camera
    public float AssistedTargetingRadius = 1f;			// the radius of the raycast that will be used to detect enemies
	public float AssistedTargetingTrackingRadius = 2f;  // when an enemy is being tracked, this is the radius threshold to stay within to keep tracking
    protected float startingAssistedTargetingTrackingRadius;
    public float AssistedTargetingDistance = Mathf.Infinity;
    public bool AssistedTargetingFoldout = false;       // for editor use



    

    protected Renderer m_Renderer = null; 				// Cache the renderer
	protected GameObject m_GameObject = null;			// cache the gameobject
	protected Color m_DefaultColor;						// cache the default crosshair color
	protected Camera m_PlayerCamera = null;				// cache the player camera
    protected vp_FPCamera m_PlayerFPCamera = null; //##Mike's Add
    protected Vector3 m_ScreenCenter = Vector3.zero;	// cache the center of the screen
	protected Vector3 m_CachedScale = Vector3.one;		// cache the original scale of the crosshair
	protected string m_CachedTextureName = "";			// original crosshairs texture name
	protected Dictionary<Collider, Transform> m_Enemies = new Dictionary<Collider, Transform>(); // for testing interactable components
    protected Dictionary<Collider, Transform> af_Enemies = new Dictionary<Collider, Transform>(); // for testing interactable components
    protected Vector3 m_CrosshairNextPosition = Vector3.zero;	// next position the crosshair should be moved to (for assisted targeting)
	protected Transform m_CurrentTrackedEnemy = null;	// enemy that is currently being tracked
    protected Transform af_CurrentTrackedEnemy = null;   // enemy that is currently being tracked
    protected bool m_Tracking = false;					// whether or not an enemy is being tracked
    protected bool af_Tracking = false;                  // whether or not an enemy is being tracked
    protected virtual bool TrackingEnemy(){ return m_Tracking; }
    protected virtual bool AutoFireEnemy() { return af_Tracking; }
    protected bool m_ShowCrosshair = true;				// should the crosshair be shown
	protected bool m_Interacting = false;				// is the player interacting
	protected vp_UITween.Handle m_ColorHandle = new vp_UITween.Handle();	// handle for the crosshairs color tween
	protected vp_UITween.Handle m_ScaleCrosshairHandle = new vp_UITween.Handle();   // handle for the crosshairs scale tween

    protected virtual Color m_CrosshairColor			// gets the color of the crosshair based on what it's over
	{
		get{
			Color color = m_DefaultColor;
			if(m_Tracking)
				color = EnemyColor;
				
			color.a = m_ShowCrosshair ? 1 : 0;
			
			return color;
		}
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected override void Awake()
	{
	
		base.Awake();
	
		m_Renderer = GetComponent<Renderer>();
		m_GameObject = gameObject;
		if (m_Renderer != null && m_Renderer.material != null && m_Renderer.material.mainTexture != null)
		{
			m_DefaultColor = m_Renderer.material.color;
			m_CachedTextureName = m_Renderer.material.mainTexture.name;
		}
        m_PlayerFPCamera = Manager.Player.GetComponentInChildren<vp_FPCamera>();
        m_PlayerCamera = m_PlayerFPCamera.GetComponent<Camera>();
		m_CachedScale = m_Transform.localScale;

        startingAssistedTargetingTrackingRadius = AssistedTargetingTrackingRadius;
        startingAutoFireContinueRadius = AutoFireContinueRadius;



    }
	
	
	/// <summary>
	/// 
	/// </summary>
	protected override void Start()
	{
	
		m_ScreenCenter = Manager.UICamera.GetComponent<Camera>().ScreenToWorldPoint( new Vector3(Screen.width * .5f, Screen.height * .5f, m_Transform.position.z) );
		
		base.Start();
	
	}
	
	
	/// <summary>
	/// adds a global event for toggling the crosshair
	/// </summary>
	protected override void OnEnable()
	{
	
		base.OnEnable();
	
		vp_GlobalEvent<bool>.Register("Display Crosshair", DisplayCrosshair);
		vp_GlobalEventReturn<bool>.Register("Tracking Enemy", TrackingEnemy);
	
	}
	
	
	/// <summary>
	/// removes the global event for toggling the crosshair
	/// </summary>
	protected override void OnDisable()
	{
	
		base.OnDisable();
	
		vp_GlobalEvent<bool>.Unregister("Display Crosshair", DisplayCrosshair);
		vp_GlobalEventReturn<bool>.Unregister("Tracking Enemy", TrackingEnemy);
	
	}
	
	
	/// <summary>
	/// updates the crosshair color
	/// </summary>
	protected override void Update()
	{
        //var currentRotation = Manager.Player.Rotation.Get();
        //Manager.Player.Rotation.Set(new Vector2(currentRotation.x + gyroAccelScript.selfUpdate.rotNew.eulerAngles.x, currentRotation.y + gyroAccelScript.selfUpdate.rotNew.eulerAngles.y));
        //gyroAccelScript.SetHeadingOffset(currentRotation.x);

        m_Interacting = (Manager.Player.Interactable.Get() != null && Manager.Player.Interactable.Get().GetType() == typeof(vp_Grab)) || Manager.Player.Climb.Active;

        if (AutoFireEnabled)
            StartAutoFireTarget();


        EnemyCheck();

		HandleCrosshairColor();
		
		if(m_CurrentTrackedEnemy == null)
			m_CrosshairNextPosition = m_ScreenCenter;

        TrackEnemy();

        if (AutoFireEnabled)
            ContinueAutoFireTarget();

        if (AssistedTargetingCrosshairSmoothing)
            m_Transform.position = Vector3.Lerp(m_Transform.position, m_CrosshairNextPosition, Time.deltaTime * AssistedTargetingSpeed);
        else
            m_Transform.position = m_CrosshairNextPosition;

        if (m_Tracking && EnemyCrosshair != null && AssistedTargetingLockCrosshair)
            Manager.Player.Crosshair.Set(EnemyCrosshair);


    }
	
	
	/// <summary>
	/// Handles the color of the crosshair.
	/// </summary>
	protected virtual void HandleCrosshairColor()
	{
	
		if(m_ShowCrosshair)
			vp_UITween.ColorTo(m_GameObject, m_CrosshairColor, ColorChangeDuration, m_ColorHandle);
		else
			m_GameObject.GetComponent<Renderer>().material.color = m_CrosshairColor;
	
	}



    /*
    /// <summary>
    /// Automatically fire at enemy
    /// </summary>
    protected virtual void AutoFire()
    {


        if (Physics.Raycast(m_PlayerCamera.transform.position, m_PlayerCamera.transform.forward, Mathf.Infinity, EnemyMask))
        {
            if (autoFireTargetAcquisitionTimeStart == -1) 
                autoFireTargetAcquisitionTimeStart = Time.time;
        }
        else
            autoFireTargetAcquisitionTimeStart = -1;

        if(autoFireTargetAcquisitionTimeStart != -1 && (Time.time - autoFireTargetAcquisitionTimeStart) >= autoFireTargetAcquisitionTime) 
            Manager.Player.Attack.TryStart();

    }
    */

    /// <summary>
    /// Checks if the crosshair is over an enemy
    /// </summary>
    protected void EnemyCheck()
    {

        if (Manager.Player.Dead.Active)
            return;

        Transform enemy = null;
        RaycastHit hit;
        if (Physics.SphereCast(m_PlayerCamera.transform.position, AssistedTargetingRadius, m_PlayerCamera.transform.forward, out hit, AssistedTargetingDistance, EnemyMask))
            //if((EnemyMask.value & 1 << hit.collider.gameObject.layer) != 0)
            if (!m_Enemies.TryGetValue(hit.collider, out enemy))
                m_Enemies.Add(hit.collider, enemy = hit.collider.transform);

        if (enemy != null)
        {
            if (AssistedTargetingRequireLineOfSight)
            {
                Vector3 head = new Vector3(enemy.GetComponent<Collider>().bounds.center.x, enemy.GetComponent<Collider>().bounds.center.y + ((enemy.GetComponent<Collider>().bounds.max.y - enemy.GetComponent<Collider>().bounds.center.y) / 2), enemy.GetComponent<Collider>().bounds.center.z);
                if (Physics.Linecast(m_PlayerCamera.transform.position, head, ~(EnemyMask | 1 << vp_Layer.Trigger)))
                {
                    if (Time.time >= AssistedTargetingLastTrackedTime + AssistedTargetingLOSTimeout)
                        m_CurrentTrackedEnemy = null;
                    else
                        m_CurrentTrackedEnemy = enemy;
                }
                else
                {

                    //Debug.Log("Tracked");
                    m_CurrentTrackedEnemy = enemy;
                    AssistedTargetingLastTrackedTime = Time.time;
                }
                //m_CurrentTrackedEnemy = Physics.Linecast(m_PlayerCamera.transform.position, head, ~(EnemyMask | 1 << vp_Layer.Trigger)) ? null : enemy;

                /*
                var distanceToTarget = (m_PlayerCamera.transform.position - head).magnitude;
                //Debug.Log(distanceToTarget);
                if (distanceToTarget < 3)
                {//lower tracking radius when target in near
                    //startingAssistedTargetingTrackingRadius = AssistedTargetingTrackingRadius;
                    AssistedTargetingTrackingRadius = 0.5f;
                }
                else
                    AssistedTargetingTrackingRadius = startingAssistedTargetingTrackingRadius;

                */
                //Debug.DrawLine(m_PlayerCamera.transform.position, head, Color.red);
            }
            else
                m_CurrentTrackedEnemy = enemy;

        }
        //else {
            //Debug.Log("TrackingRadius Reset");
            //AssistedTargetingTrackingRadius = startingAssistedTargetingTrackingRadius;
        //}

    }

    /// <summary>
    /// Tracks the enemy.
    /// </summary>
    protected virtual void TrackEnemy()
	{
		
		m_Tracking = false;
	
		if(m_CurrentTrackedEnemy == null || Manager.Player.Dead.Active)
			return;
			
		Transform enemy = null;
		RaycastHit[] hits = Physics.SphereCastAll( m_PlayerCamera.transform.position, AssistedTargetingTrackingRadius, m_PlayerCamera.transform.forward, AssistedTargetingDistance);
		foreach(RaycastHit hit in hits)
		{
			if(!m_Enemies.TryGetValue(hit.collider, out enemy))
				m_Enemies.Add(hit.collider, enemy = hit.collider.transform);

            if (enemy != null && enemy == m_CurrentTrackedEnemy)
            {
                m_Tracking = true;

                if (AssistedTargetingEnabled)
                {
                    // position crosshair
                    Vector3 head = new Vector3(m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.x, m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.y + ((m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.max.y - m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.y) / 2), m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.z);
                    Vector3 viewportPos = m_PlayerCamera.WorldToViewportPoint(head);
                    viewportPos.z = m_ScreenCenter.z;
                    m_CrosshairNextPosition = Manager.UICamera.ViewportToWorldPoint(viewportPos);

                    //Mike's Add
                    if (hammerOnTarget)
                        if (menuScript.rightHammerOnSettings.targetAxis != "Attack")
                        {
                            previousHammerOnAxis = menuScript.rightHammerOnSettings.targetAxis;
                            menuScript.rightHammerOnSettings.targetAxis = "Attack";
                            menuScript.rightHammerOnSettings.transform.GetChild(0).gameObject.SetActive(true);
                        }

                    Quaternion newRotation = Quaternion.Slerp(m_PlayerCamera.transform.rotation, Quaternion.LookRotation(head - m_PlayerCamera.transform.position), Time.deltaTime * AssistedTargetingCameraSpeed);
                    Manager.Player.Rotation.Set(new Vector2(newRotation.eulerAngles.x, newRotation.eulerAngles.y));

                    /*
                    var distanceToTarget = (m_PlayerCamera.transform.position - head).magnitude;

                    if (distanceToTarget < 3)
                    {
                        AssistedTargetingTrackingRadius = 0.5f;
                    }
                    else
                        AssistedTargetingTrackingRadius = startingAssistedTargetingTrackingRadius;
                    */
                }
            }
            //else
                //AssistedTargetingTrackingRadius = startingAssistedTargetingTrackingRadius;

        }

        if (!m_Tracking) {

            //Mike's Add
            if (hammerOnTarget)
            {
                menuScript.rightHammerOnSettings.targetAxis = previousHammerOnAxis;
                if (previousHammerOnAxis == "None")
                    menuScript.rightHammerOnSettings.transform.GetChild(0).gameObject.SetActive(false);
            }

            m_CurrentTrackedEnemy = null;
        }
    }



    protected void StartAutoFireTarget()
    {
        if (Manager.Player.Dead.Active)
            return;

        Transform enemy = null;
        RaycastHit hit;
        if (Physics.SphereCast(m_PlayerCamera.transform.position, AutoFireStartRadius, m_PlayerCamera.transform.forward, out hit, AutoFireDistance, EnemyMask))
            if (!af_Enemies.TryGetValue(hit.collider, out enemy))
                af_Enemies.Add(hit.collider, enemy = hit.collider.transform);

        if (enemy != null)
        {
            if (AutoFireRequireLineOfSight)
            {
                Vector3 head = new Vector3(enemy.GetComponent<Collider>().bounds.center.x, enemy.GetComponent<Collider>().bounds.center.y + ((enemy.GetComponent<Collider>().bounds.max.y - enemy.GetComponent<Collider>().bounds.center.y) / 2), enemy.GetComponent<Collider>().bounds.center.z);
                if (Physics.Linecast(m_PlayerCamera.transform.position, head, ~(EnemyMask | 1 << vp_Layer.Trigger)))
                {
                    if (Time.time >= AutoFireLastTrackedTime + AutoFireLOSTimeout)
                        af_CurrentTrackedEnemy = null;
                    else
                        af_CurrentTrackedEnemy = enemy;
                }
                else
                {
                    af_CurrentTrackedEnemy = enemy;
                    AutoFireLastTrackedTime = Time.time;
                }

            }
            else
                af_CurrentTrackedEnemy = enemy;

        }



    }


    protected virtual void ContinueAutoFireTarget()
    {

        af_Tracking = false;

        if (af_CurrentTrackedEnemy == null || Manager.Player.Dead.Active)
            return;

        Transform enemy = null;
        RaycastHit[] hits = Physics.SphereCastAll(m_PlayerCamera.transform.position, AutoFireContinueRadius, m_PlayerCamera.transform.forward, AutoFireDistance);
        foreach (RaycastHit hit in hits)
        {
            if (!af_Enemies.TryGetValue(hit.collider, out enemy))
                af_Enemies.Add(hit.collider, enemy = hit.collider.transform);

            if (enemy != null && enemy == af_CurrentTrackedEnemy)
            {
                af_Tracking = true;

                AutoFireCrosshair.fillAmount = Mathf.Clamp(((Time.time - AutoFireTargetAcquisitionTimeStart) / AutoFireTargetAcquisitionTime), 0, 1);

                if (AutoFireTargetAcquisitionTimeStart == -1)
                    AutoFireTargetAcquisitionTimeStart = Time.time;

                if ((Time.time - AutoFireTargetAcquisitionTimeStart) >= AutoFireTargetAcquisitionTime)
                    AutoFiring = true;
                //Manager.Player.Attack.TryStart();

                /*
                var distanceToTarget = (m_PlayerCamera.transform.position - head).magnitude;

                if (distanceToTarget < 3)
                {//lower tracking radius when target in near
                    //startingAutoFireContinueRadius = AutoFireContinueRadius;
                    AutoFireContinueRadius = 0.5f;
                }
                else
                    AutoFireContinueRadius = startingAutoFireContinueRadius;
                */

            }
        }

        if (!af_Tracking)
        {
            af_CurrentTrackedEnemy = null;
            AutoFireTargetAcquisitionTimeStart = -1;
            AutoFireCrosshair.fillAmount = 0;
            AutoFiring = false;
        }
    }



    /// <summary>
    /// Hide crosshair when climbing starts
    /// </summary>
    protected virtual void OnStart_Climb(){ DisplayCrosshair(false); }
	
	
	/// <summary>
	/// Show crosshair when climbing stops
	/// </summary>
	protected virtual void OnStop_Climb(){ DisplayCrosshair(); }
	
	
	/// <summary>
	/// Hide crosshair if zooming
	/// </summary>
	protected virtual void OnStart_Zoom(){ DisplayCrosshair(false); }
	
	
	/// <summary>
	/// Show crosshair when zoom stopped
	/// </summary>
	protected virtual void OnStop_Zoom(){ DisplayCrosshair(); }
	
	protected virtual void OnStart_Run()
	{
	
		if(Manager.Player.CanInteract.Get() || !ScaleOnRun)
			return;
			
		vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", m_CachedScale * ScaleMultiplier, "duration", .15f, "handle", m_ScaleCrosshairHandle) );
		
	}
	
	
	/// <summary>
	/// scale the crosshair to origin when running stops
	/// </summary>
	protected virtual void OnStop_Run()
	{
	
		if(Manager.Player.CanInteract.Get() || !ScaleOnRun)
			return;
	
		vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", m_CachedScale, "duration", .15f, "handle", m_ScaleCrosshairHandle) );
		
	}
	
	
	/// <summary>
	/// Displays the crosshair based on the specified value.
	/// </summary>
	protected virtual void DisplayCrosshair( bool val = true )
	{
	
		m_ShowCrosshair = val;
	
	}
    /*
    IEnumerator AssistedTargetingLOSTimeoutCheck()
    {
        AssistedTargetingLOSTimeoutExpired = false;
        yield return new WaitForSeconds(AssistedTargetingLOSTimeout);
        AssistedTargetingLOSTimeoutExpired = true;
    }
    */


	
	
	/// <summary>
	/// Gets or sets the value of the Crosshair texture
	/// </summary>
	protected virtual Texture OnValue_Crosshair
	{
		get { return m_Renderer.material.mainTexture; }
		set {
			if(m_Tracking && EnemyCrosshair != null)
			{
				m_Renderer.material.mainTexture = EnemyCrosshair;
			}
			else if(value.name == "")
				m_ShowCrosshair = false;
			else
			{
				if(!Manager.Player.Zoom.Active)
					m_ShowCrosshair = true;
				m_Renderer.material.mainTexture = value;
			}
			
			if(m_Tracking)
				return;
		
			// change the icon size if it's not the default 
			Vector3 localScale = value.name == m_CachedTextureName ? m_CachedScale : m_CachedScale * InteractIconSize;
			
			// make the icon size bigger if no icon shown and grabbing
			localScale = Manager.Player.Interactable.Get() != null && Manager.Player.Interactable.Get().GetType() == typeof(vp_Grab) && value.name == "" ? m_CachedScale * (InteractIconSize * 3) : localScale;
			vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", localScale, "duration", 0, "handle", m_ScaleCrosshairHandle) );
		}
	}

}

