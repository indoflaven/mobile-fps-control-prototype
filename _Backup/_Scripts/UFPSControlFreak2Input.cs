﻿#if !CF2_DEV_PROJECT

using UnityEngine;
using ControlFreak2;
using MSP_Input;

#if UNITY_EDITOR
using UnityEditor;
using ControlFreak2Editor;
#endif


public class UFPSControlFreak2Input : vp_FPInput
	{
    // --------------


    protected GameObject MSP_InputExists = null; //check to see if Gyro control is active in scene
    protected GameObject menuLayer = null; 
    protected vp_UIManager uiManager; 

    protected override void Start()
		{
		base.Start();

		this.MouseCursorBlocksMouseLook = false;
        
		this.SyncControlVisibility(true);

        //CF2Input.CalibrateTilt();

        MSP_InputExists = GameObject.Find("MSP_Input");

        uiManager = GameObject.Find("RootUI").GetComponent<vp_UIManager>();


        menuLayer = GameObject.Find("MenuLayer");




    }

	// ----------------
	private void SyncControlVisibility(bool skipAnim)
		{
		InputRig rig = CF2Input.activeRig;
		if (rig == null)
			return;

		bool
			attackOn				= true,
			zoomOn				= true,
			reloadOn				= true,
			interactOn			= true,
			runOn					= true,
			weaponSelectOn		= true;

		if (this.FPPlayer.Climb.Active)
			{
			runOn = false;	
			zoomOn = false;			
			reloadOn = false;			
			}

		if ((this.FPPlayer.CurrentWeaponIndex.Get() <= 0) || (this.FPPlayer.CurrentWeaponMaxAmmoCount.Get() <= 0))
			{
			zoomOn = false;			
			reloadOn = false;			

			if (this.FPPlayer.CurrentWeaponIndex.Get() <= 0)
				attackOn = false;						
			}

		
		if (!this.FPPlayer.CanInteract.Get())
			interactOn = false;			


		rig.SetSwitchState("Gameplay",		this.AllowGameplayInput && !vp_Gameplay.IsPaused);
		rig.SetSwitchState("Zoom",				zoomOn);
		rig.SetSwitchState("Reload",			reloadOn);
		rig.SetSwitchState("Interact",		interactOn);
		rig.SetSwitchState("Run",				runOn);
		rig.SetSwitchState("Attack",			attackOn);
		rig.SetSwitchState("WeaponSelect",	weaponSelectOn);
						
		rig.ApplySwitches(skipAnim);
		}


	// --------------
	override protected void UpdateCursorLock()
		{
		if (vp_Utility.LockCursor)
			vp_Utility.LockCursor = false;
		}

	
	// -----------------
	protected override void Update()
		{
		this.SyncControlVisibility(true);

		base.Update();
		}


	// ---------------------
	protected override void InputInteract()
		{
		if (CF2Input.GetButtonDown("Interact"))
			FPPlayer.Interact.TryStart();
		else
			FPPlayer.Interact.TryStop();
		}


    // -------------------
	protected override void InputMove()
		{
		FPPlayer.InputMoveVector.Set(new Vector2(CF2Input.GetAxisRaw("Horizontal"), CF2Input.GetAxisRaw("Vertical")));
		}



    /*
    public float skidTime = .2f;
    protected TouchJoystick moveJoystick;
    protected Vector2 lastMoveVector;
    protected Vector2 lastLastMoveVector;
    protected Vector2 skidMoveVector;
    protected bool skidding = false;
    // -------------------
    protected override void InputMove()
    {
        var currentMoveVector = new Vector2(CF2Input.GetAxisRaw("Horizontal"), CF2Input.GetAxisRaw("Vertical"));
        //Debug.Log(CF2Input.activeRig.FindTouchControl("Joystick-Walk").IsActiveAndVisible());
        //FPPlayer.InputMoveVector.Set(currentMoveVector);


        if (CF2Input.activeRig.FindTouchControl("Joystick-Walk").IsActiveButInvisible() && lastLastMoveVector != Vector2.zero) //capture the frame the player goes from moving to stopped
        {
            skidding = true;
            skidMoveVector = lastLastMoveVector;
            vp_Timer.In(skidTime, stopSkidding);
        }

        if (skidding)
        {
            Debug.Log("Skidding: " + skidMoveVector);
            FPPlayer.InputMoveVector.Set(skidMoveVector);
        }
        else
        {
            FPPlayer.InputMoveVector.Set(currentMoveVector);
        }

        lastLastMoveVector = lastMoveVector;
        lastMoveVector = currentMoveVector;


    }

    void stopSkidding()
    {
        skidding = false;
        skidMoveVector = Vector2.zero;
    }
    */
    

    // ---------------------
    protected override void InputRun()
		{
		if (CF2Input.GetButton("Run"))
			FPPlayer.Run.TryStart();
		else
			FPPlayer.Run.TryStop();
		}


	// ------------------
	protected override void InputJump()
		{
		if (CF2Input.GetButton("Jump"))
			FPPlayer.Jump.TryStart();
		else
			FPPlayer.Jump.Stop();
		}

    //##Mike's Add
    // ------------------
    protected override void InputBlink()
    {
        if (CF2Input.GetButton("Blink"))
        {
            Debug.Log("Blink Button");
            FPPlayer.Blink.TryStart();
        }
            
        else
            FPPlayer.Blink.Stop();
    }


    // ----------------
    protected override void InputCrouch()
		{
		if (CF2Input.GetButton("Crouch"))
			FPPlayer.Crouch.TryStart();
		else
			FPPlayer.Crouch.TryStop();
		}


	// ----------------
	protected override void InputCamera()
		{
		if (CF2Input.GetButton("Zoom"))
			FPPlayer.Zoom.TryStart();
		else
			FPPlayer.Zoom.TryStop();

		}


	// ----------------
	protected override void InputAttack()
		{
		if (CF2Input.GetButton("Attack"))
			FPPlayer.Attack.TryStart();
		else
			FPPlayer.Attack.TryStop();

		}


    //##Mike's Add
    // ------------------
    protected override void InputFlash()
    {
        if (CF2Input.GetButton("Flash"))
        {
            FPPlayer.SetWeapon.TryStart(5);
            FPPlayer.SetWeapon.TryStop();
            FPPlayer.Attack.Start();
            vp_Timer.In(1.0f, RestoreWeapon);
  
        }
            
        //else
            //FPPlayer.Attack.Stop();
    }


    //##Mike's Add
    // ------------------

    private vp_Timer.Handle meleeAttackTimer = new vp_Timer.Handle();
    private vp_Timer.Handle meleeRestoreWeaponTimer = new vp_Timer.Handle();
    private vp_Timer.Handle endMeleeAttackTimer = new vp_Timer.Handle();
    private bool currentMeleeAttack = false;

    protected override void InputQuickMelee()
    {
        if (CF2Input.GetButton("QuickMelee") && !currentMeleeAttack)
        {
            Debug.Log("melee attack!");

            currentMeleeAttack = true;
            FPPlayer.SetWeapon.Stop();
            FPPlayer.SetWeapon.TryStart(4);
            
            vp_Timer.In(.1f, StartMeleeAttack, meleeAttackTimer);

        }

        //else
        //FPPlayer.Attack.Stop();
    }


    void StartMeleeAttack()
    {
        FPPlayer.SetWeapon.Stop();
        FPPlayer.Attack.Start();
        vp_Timer.In(.5f, meleeRestoreWeapon, meleeRestoreWeaponTimer);
    }
    void meleeRestoreWeapon()
    {
        FPPlayer.Attack.Stop();
        FPPlayer.SetWeapon.TryStart(2);
        vp_Timer.In(.1f, endMeleeAttack, endMeleeAttackTimer);
    }

    void endMeleeAttack()
    {
        currentMeleeAttack = false;
    }


    void RestoreWeapon()
    {
        FPPlayer.Attack.Stop();
        FPPlayer.SetWeapon.TryStart(2);
        //vp_Timer.In(.5f, WeaponRestored, weaponRestoredTimer);
    }



    //##Mike's Add
    // ------------------
    protected bool quickFiring = false;

    protected override void InputQuickFire()
    {
        if (CF2Input.GetButton("QuickFire"))
        {
            //Debug.Log("QuickFire");

            if (!quickFiring) {

                quickFiring = true;
                //if(!vp_LocalPlayer.WeaponHandler.CurrentShooter.StateEnabled("QuickFire"))
                vp_LocalPlayer.WeaponHandler.CurrentShooter.SetState("QuickFire");
                FPPlayer.Attack.TryStart();
                for(var i = 1; i < vp_LocalPlayer.CurrentAmmo; i++)
                    vp_Timer.In(vp_LocalPlayer.WeaponHandler.CurrentShooter.ProjectileFiringRate*i, quickFireRepeater);

                vp_Timer.In(vp_LocalPlayer.WeaponHandler.CurrentShooter.ProjectileFiringRate * vp_LocalPlayer.CurrentAmmo, restoreFireRate);

            }

        }

        //else
        //FPPlayer.Attack.Stop();
    }

    void quickFireRepeater()
    {
        //Debug.Log("QuickFire Shot");
        FPPlayer.Attack.Start(1f);

    }

    void restoreFireRate()
    {
        //Debug.Log("Restore Firerate");
        vp_LocalPlayer.WeaponHandler.CurrentShooter.ResetState();
        quickFiring = false;

    }

    // --------------
    protected override void InputReload()
		{
		if (CF2Input.GetButtonDown("Reload"))
			FPPlayer.Reload.TryStart();
            
    }


    // ---------------------
    protected override void InputSetWeapon()
		{
		if (CF2Input.GetButtonDown("SetPrevWeapon"))
			FPPlayer.SetPrevWeapon.Try();

		if (CF2Input.GetButtonDown("SetNextWeapon"))
			FPPlayer.SetNextWeapon.Try();


		if (CF2Input.GetButtonDown("ClearWeapon"))
			FPPlayer.SetWeapon.TryStart(0);

		}


	// -----------------
	protected override void UpdatePause()
	{
		if (CF2Input.GetButtonDown("Pause"))
        {
            //FPPlayer.Pause.Set(!FPPlayer.Pause.Get());
            MenuButtonOnClick();
        }

    }

    public void MenuButtonOnClick()
    {

        if (!menuLayer.activeSelf)
        {
            menuLayer.SetActive(true);

        }
        else
        {
            menuLayer.SetActive(false);
            //vp_Timer.In(.2f, restoreMouseLook); //Hack to restore mouselook after pause
        }

    }

    void restoreMouseLook()  //Hack to restore mouselook after pause
    {
        Debug.Log("Restore Mouselook");
        uiManager.Player.Rotation.Set(new Vector2(0, 0));
    }


    /*
    // -------------
    protected override Vector2 GetMouseLook()
		{

        if (MouseCursorBlocksMouseLook) // && !vp_Utility.LockCursor)
			return Vector2.zero;

		if (m_LastMouseLookFrame == Time.frameCount)
			return m_CurrentMouseLook;

		m_LastMouseLookFrame = Time.frameCount;

		m_MouseLookSmoothMove.x = CF2Input.GetAxisRaw("Mouse X") * Time.timeScale;
		m_MouseLookSmoothMove.y = CF2Input.GetAxisRaw("Mouse Y") * Time.timeScale;

		m_CurrentMouseLook = m_MouseLookSmoothMove;

		float mouseAcceleration = 0.0f;

		float accX = Mathf.Abs(m_CurrentMouseLook.x);
		float accY = Mathf.Abs(m_CurrentMouseLook.y);

		if (MouseLookAcceleration)
			{
			mouseAcceleration = Mathf.Sqrt((accX * accX) + (accY * accY)) / Delta;
			mouseAcceleration = (mouseAcceleration <= MouseLookAccelerationThreshold) ? 0.0f : mouseAcceleration;
			}
        
		m_CurrentMouseLook.x *= (MouseLookSensitivity.x + mouseAcceleration);
        m_CurrentMouseLook.y *= (MouseLookSensitivity.y + mouseAcceleration);

        if (MouseLookSensitivity.x == 0) //##Mike's Add - kill accelleration too if sensitivity is zero
            m_CurrentMouseLook.x = 0;
        if (MouseLookSensitivity.y == 0)
            m_CurrentMouseLook.y = 0;


        m_CurrentMouseLook.y = (MouseLookInvert ? m_CurrentMouseLook.y : -m_CurrentMouseLook.y);

        if(MSP_InputExists.activeSelf) { 
            GyroAccel.AddFloatToHeadingOffset(m_CurrentMouseLook.x);
            GyroAccel.AddFloatToPitchOffset(-m_CurrentMouseLook.y);
            m_CurrentMouseLook = new Vector2(0, 0);
        }

        return m_CurrentMouseLook;
		}

    */
    
    /// <summary>
    /// mouselook implementation with smooth filtering and acceleration
    /// </summary>
    protected override Vector2 GetMouseLook()
    {

        // don't allow mouselook if we are using the mouse cursor
        if (MouseCursorBlocksMouseLook) // && !vp_Utility.LockCursor)
            return Vector2.zero;

        // only recalculate mouselook once per frame or smoothing will break
        if (m_LastMouseLookFrame == Time.frameCount)
            return m_CurrentMouseLook;

        m_LastMouseLookFrame = Time.frameCount;

        // --- fetch mouse input ---

        m_MouseLookSmoothMove.x = CF2Input.GetAxisRaw("Mouse X") * Time.timeScale;
        m_MouseLookSmoothMove.y = CF2Input.GetAxisRaw("Mouse Y") * Time.timeScale;


        // --- mouse smoothing ---

        // make sure the defined smoothing vars are within range
        MouseLookSmoothSteps = Mathf.Clamp(MouseLookSmoothSteps, 1, 20);
        MouseLookSmoothWeight = Mathf.Clamp01(MouseLookSmoothWeight);

        // keep mousebuffer at a maximum of (MouseSmoothSteps + 1) values
        while (m_MouseLookSmoothBuffer.Count > MouseLookSmoothSteps)
            m_MouseLookSmoothBuffer.RemoveAt(0);

        // add current input to mouse input buffer
        m_MouseLookSmoothBuffer.Add(m_MouseLookSmoothMove);

        // calculate mouse smoothing
        float weight = 1;
        Vector2 average = Vector2.zero;
        float averageTotal = 0.0f;
        for (int i = m_MouseLookSmoothBuffer.Count - 1; i > 0; i--)
        {
            average += m_MouseLookSmoothBuffer[i] * weight;
            averageTotal += (1.0f * weight);
            weight *= (MouseLookSmoothWeight / Delta);
        }

        // store the averaged input value
        averageTotal = Mathf.Max(1, averageTotal);
        m_CurrentMouseLook = vp_MathUtility.NaNSafeVector2(average / averageTotal);

        // --- mouse acceleration ---

        float mouseAcceleration = 0.0f;

        float accX = Mathf.Abs(m_CurrentMouseLook.x);
        float accY = Mathf.Abs(m_CurrentMouseLook.y);

        if (MouseLookAcceleration)
        {
            mouseAcceleration = Mathf.Sqrt((accX * accX) + (accY * accY)) / Delta;
            mouseAcceleration = ((mouseAcceleration <= MouseLookAccelerationThreshold) || MouseLookAccelerationThreshold == 0.0f) ? 0.0f : mouseAcceleration;
        }

        m_CurrentMouseLook.x *= (MouseLookSensitivity.x + mouseAcceleration);
        m_CurrentMouseLook.y *= (MouseLookSensitivity.y + mouseAcceleration);

        m_CurrentMouseLook.y = (MouseLookInvert ? m_CurrentMouseLook.y : -m_CurrentMouseLook.y);

        
        if (MSP_InputExists.activeSelf)
        {
            GyroAccel.AddFloatToHeadingOffset(m_CurrentMouseLook.x);
            GyroAccel.AddFloatToPitchOffset(-m_CurrentMouseLook.y);
            m_CurrentMouseLook = new Vector2(0, 0);
        }
        
        
        return m_CurrentMouseLook;

    }

    protected override Vector2 GetMouseLookRaw()
		{
		if (MouseCursorBlocksMouseLook) // && !vp_Utility.LockCursor)
			return Vector2.zero;

		m_MouseLookRawMove.x = CF2Input.GetAxisRaw("Mouse X");
		m_MouseLookRawMove.y = CF2Input.GetAxisRaw("Mouse Y");


        return m_MouseLookRawMove;
		}

	protected override float OnValue_InputClimbVector
		{ get { return CF2Input.GetAxisRaw("Vertical"); } }

	protected override bool OnMessage_InputGetButton(string button)
		{ return CF2Input.GetButton(button); }


	protected override bool OnMessage_InputGetButtonUp(string button)
		{ return CF2Input.GetButtonUp(button); }


	protected override bool OnMessage_InputGetButtonDown(string button)
		{ return CF2Input.GetButtonDown(button); }



#if UNITY_EDITOR

 	const string 
		dialogTitle = "UFPS-CF2 Setup";

	[MenuItem(CFEditorUtils.INTEGRATIONS_MENU_PATH + "UFPS/Add CF2 Input to selected object.", false, CFEditorUtils.INTEGRATIONS_MENU_PRIO)]
	static private void AddToSelected()
		{
		if (Selection.activeTransform == null)
			{
			EditorUtility.DisplayDialog(dialogTitle, "Nothing is selected!", "OK");
			return;
			}

		AddToObject(Selection.activeTransform);
		}

	[MenuItem(CFEditorUtils.INTEGRATIONS_MENU_PATH + "UFPS/Find existing vp_FPInput and add CF2 Input", false, CFEditorUtils.INTEGRATIONS_MENU_PRIO)]
	static private void AddToExistingPlayerInput()
		{
		var obj = GameObject.FindObjectOfType<vp_FPInput>();

		if (obj == null)
			{
			EditorUtility.DisplayDialog(dialogTitle, "There's no vp_FPInput in the scene!", "OK");
			return;
			}

		AddToObject(obj.transform);
		}


	// -------------------
	static private void AddToObject(Transform obj)
		{
		if (obj == null)
			return;


		// Look for other vp_FPInput components.

		vp_FPInput[] 
			inputList = obj.GetComponents<vp_FPInput>();
		UFPSControlFreak2Input 
			existingComponent = null;
				
		if (inputList != null) 
			{
			for (int i = 0; i < inputList.Length; ++i) 
				{
				if (inputList[i] is UFPSControlFreak2Input)
					{
					existingComponent = inputList[i] as UFPSControlFreak2Input;
					break;
					}
				}
			}

		if ((existingComponent != null) && existingComponent.enabled && (inputList.Length == 1))
			{
			EditorUtility.DisplayDialog(dialogTitle, "CF2 Input is already added!", "OK");
			return;
			}

		string undoLabel = ((existingComponent == null) ? "Add ControlFreak2Input" : "Re-enable ControlFreak2Input");

		Undo.SetCurrentGroupName(undoLabel);
			  
		// Remove other PlayeInputs.

		int componentsRemoved = 0;
		if (inputList != null)
			{
			for (int i = 0; i < inputList.Length; ++i)
				{
				if (inputList[i] == existingComponent)
					continue;

				Undo.DestroyObjectImmediate(inputList[i]);	
				++componentsRemoved;
				}
			}

		// Re-enable existing component...

		if ((existingComponent != null) && !existingComponent.enabled)
			{
			Undo.RecordObject(existingComponent, undoLabel);
			existingComponent.enabled = true;
			EditorUtility.SetDirty(existingComponent);
			}
				
		// Create ControlFreak2Input if missing...
		
		if (existingComponent == null)
			{
			UFPSControlFreak2Input c = obj.gameObject.AddComponent<UFPSControlFreak2Input>();
			Undo.RegisterCreatedObjectUndo(c, undoLabel);
			}

		Undo.FlushUndoRecordObjects(); 
	
		EditorUtility.DisplayDialog(dialogTitle, ("[" + obj.name + "] : " +
					((existingComponent == null) ? "Added" : "Re-enabled") + " ControlFreak2Input component" +
					((componentsRemoved > 0) ? " (and removed other Input components)" : "") + "."), "OK");
		}




#endif


	}

#endif
