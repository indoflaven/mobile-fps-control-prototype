﻿/////////////////////////////////////////////////////////////////////////////////
//
//	vp_UICrosshair.cs
//	© Opsive. All Rights Reserved.
//	https://twitter.com/Opsive
//	http://www.opsive.com
//
//	description:	this script manages a textured crosshair on a mesh
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using MSP_Input;
using ControlFreak2;


public class vp_UICrosshair : vp_UIControl
{

    public Color EnemyColor = Color.red;				// color of crosshair when over an enemy
	public float ColorChangeDuration = .25f;			// speed at which the color is changed
	public LayerMask EnemyMask = 1 << vp_Layer.Enemy;	// enemy layers
	public float InteractIconSize = 2;					// size that this gameobject will change to when interaction is available
	public Texture EnemyCrosshair = null;				// textre that will be changed to when crosshair is over an enemy
	public bool ScaleOnRun = true;						// should the crosshair scale when running is active
	public float ScaleMultiplier = 1.25f;				// multiplies by the original crosshair size for ScaleOnRun
	public bool AssistedTargeting = true;               // should assisted targeting be allowed
    public bool AssistedTargetingRequireLineOfSight = true; //is line of sight required to aquire and keep target
    public float AssistedTargetingLOSTimeout = 0;       //How long after line of sight is lost should assisted targeting stop
    protected float LastTrackedTime = 0;                //Has the LOS Timeout expired yet?
    public bool AssistedTargetingLockCrosshair = true;  //Show the lock on crosshair?
    public bool AssistedTargetingCrosshairSmoothing = false;//Use Assisted Targeting Speed?
    public float AssistedTargetingSpeed = 20;			// speed of assisted targeting interpolation of the crosshair
    public float AssistedTargetingCameraSpeed = 3;      // speed of assisted targeting interpolation of the camera
    public float AssistedTargetingRadius = 1f;			// the radius of the raycast that will be used to detect enemies
	public float AssistedTargetingTrackingRadius = 2f;	// when an enemy is being tracked, this is the radius threshold to stay within to keep tracking
	public bool AssistedTargetingFoldout = false;		// for editor use

    protected float startingAssistedTargetingTrackingRadius;

    protected Renderer m_Renderer = null; 				// Cache the renderer
	protected GameObject m_GameObject = null;			// cache the gameobject
	protected Color m_DefaultColor;						// cache the default crosshair color
	protected Camera m_PlayerCamera = null;				// cache the player camera
	protected Vector3 m_ScreenCenter = Vector3.zero;	// cache the center of the screen
	protected Vector3 m_CachedScale = Vector3.one;		// cache the original scale of the crosshair
	protected string m_CachedTextureName = "";			// original crosshairs texture name
	protected Dictionary<Collider, Transform> m_Enemies = new Dictionary<Collider, Transform>(); // for testing interactable components
	protected Vector3 m_CrosshairNextPosition = Vector3.zero;	// next position the crosshair should be moved to (for assisted targeting)
	protected Transform m_CurrentTrackedEnemy = null;	// enemy that is currently being tracked
	protected bool m_Tracking = false;					// whether or not an enemy is being tracked
	protected virtual bool TrackingEnemy(){ return m_Tracking; }
	protected bool m_ShowCrosshair = true;				// should the crosshair be shown
	protected bool m_Interacting = false;				// is the player interacting
	protected vp_UITween.Handle m_ColorHandle = new vp_UITween.Handle();	// handle for the crosshairs color tween
	protected vp_UITween.Handle m_ScaleCrosshairHandle = new vp_UITween.Handle();   // handle for the crosshairs scale tween

    protected GameObject MSP_InputExists = null; //check to see if Gyro control is active in scene

    protected virtual Color m_CrosshairColor			// gets the color of the crosshair based on what it's over
	{
		get{
			Color color = m_DefaultColor;
			if(m_Tracking)
				color = EnemyColor;
				
			color.a = m_ShowCrosshair ? 1 : 0;
			
			return color;
		}
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected override void Awake()
	{
	
		base.Awake();
	
		m_Renderer = GetComponent<Renderer>();
		m_GameObject = gameObject;
		if (m_Renderer != null && m_Renderer.material != null && m_Renderer.material.mainTexture != null)
		{
			m_DefaultColor = m_Renderer.material.color;
			m_CachedTextureName = m_Renderer.material.mainTexture.name;
		}
		m_PlayerCamera = Manager.Player.GetComponentInChildren<vp_FPCamera>().GetComponent<Camera>();
		m_CachedScale = m_Transform.localScale;

        MSP_InputExists = GameObject.Find("MSP_Input");

        startingAssistedTargetingTrackingRadius = AssistedTargetingTrackingRadius;

    }
	
	
	/// <summary>
	/// 
	/// </summary>
	protected override void Start()
	{
	
		m_ScreenCenter = Manager.UICamera.GetComponent<Camera>().ScreenToWorldPoint( new Vector3(Screen.width * .5f, Screen.height * .5f, m_Transform.position.z) );
		
		base.Start();
	
	}
	
	
	/// <summary>
	/// adds a global event for toggling the crosshair
	/// </summary>
	protected override void OnEnable()
	{
	
		base.OnEnable();
	
		vp_GlobalEvent<bool>.Register("Display Crosshair", DisplayCrosshair);
		vp_GlobalEventReturn<bool>.Register("Tracking Enemy", TrackingEnemy);
	
	}
	
	
	/// <summary>
	/// removes the global event for toggling the crosshair
	/// </summary>
	protected override void OnDisable()
	{
	
		base.OnDisable();
	
		vp_GlobalEvent<bool>.Unregister("Display Crosshair", DisplayCrosshair);
		vp_GlobalEventReturn<bool>.Unregister("Tracking Enemy", TrackingEnemy);
	
	}
	
	
	/// <summary>
	/// updates the crosshair color
	/// </summary>
	protected override void Update()
	{
        //var currentRotation = Manager.Player.Rotation.Get();
        //Manager.Player.Rotation.Set(new Vector2(currentRotation.x + gyroAccelScript.selfUpdate.rotNew.eulerAngles.x, currentRotation.y + gyroAccelScript.selfUpdate.rotNew.eulerAngles.y));
        //gyroAccelScript.SetHeadingOffset(currentRotation.x);

        m_Interacting = (Manager.Player.Interactable.Get() != null && Manager.Player.Interactable.Get().GetType() == typeof(vp_Grab)) || Manager.Player.Climb.Active;
	    
		EnemyCheck();

		HandleCrosshairColor();
		
		if(m_CurrentTrackedEnemy == null)
			m_CrosshairNextPosition = m_ScreenCenter;

        TrackEnemy();

        if (AssistedTargetingCrosshairSmoothing)
            m_Transform.position = Vector3.Lerp(m_Transform.position, m_CrosshairNextPosition, Time.deltaTime * AssistedTargetingSpeed);
        else
            m_Transform.position = m_CrosshairNextPosition;

        if (m_Tracking && EnemyCrosshair != null && AssistedTargetingLockCrosshair)
			Manager.Player.Crosshair.Set(EnemyCrosshair);
	
	}
	
	
	/// <summary>
	/// Handles the color of the crosshair.
	/// </summary>
	protected virtual void HandleCrosshairColor()
	{
	
		if(m_ShowCrosshair)
			vp_UITween.ColorTo(m_GameObject, m_CrosshairColor, ColorChangeDuration, m_ColorHandle);
		else
			m_GameObject.GetComponent<Renderer>().material.color = m_CrosshairColor;
	
	}
	
	
	/// <summary>
	/// Tracks the enemy.
	/// </summary>
	protected virtual void TrackEnemy()
	{
		
		m_Tracking = false;
	
		if(m_CurrentTrackedEnemy == null || Manager.Player.Dead.Active)
			return;
			
		Transform enemy = null;
		RaycastHit[] hits = Physics.SphereCastAll( m_PlayerCamera.transform.position, AssistedTargetingTrackingRadius, m_PlayerCamera.transform.forward, Mathf.Infinity);
		foreach(RaycastHit hit in hits)
		{
			if(!m_Enemies.TryGetValue(hit.collider, out enemy))
				m_Enemies.Add(hit.collider, enemy = hit.collider.transform);
				
			if(enemy != null && enemy == m_CurrentTrackedEnemy)
			{
				m_Tracking = true;
				
				if(AssistedTargeting)
				{
					// position crosshair
					Vector3 head = new Vector3(m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.x, m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.y + ((m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.max.y - m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.y) / 2), m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center.z);
					Vector3 viewportPos = m_PlayerCamera.WorldToViewportPoint( head );
					viewportPos.z = m_ScreenCenter.z;
					m_CrosshairNextPosition = Manager.UICamera.ViewportToWorldPoint(viewportPos);

                    if (MSP_InputExists.activeSelf)
                    {
                        Vector3 targetDir = head - m_PlayerCamera.transform.position;
                        targetDir = targetDir.normalized;
                        Vector3 forwardDir = m_PlayerCamera.transform.forward;
                        forwardDir = forwardDir.normalized;

                        var headingTargetDir = Mathf.Atan2(targetDir.x, -targetDir.z);
                        var pitchTargetDir = Mathf.Asin(-targetDir.y);

                        var headingForwardDir = Mathf.Atan2(forwardDir.x, -forwardDir.z);
                        var pitchForwardDir = Mathf.Asin(-forwardDir.y);

                        float headingDelta = headingDelta = headingTargetDir - headingForwardDir;
                        if (headingTargetDir > 0 && headingForwardDir < 0 || headingTargetDir < 0 && headingForwardDir > 0)
                        {
                            Debug.Log("Crazy Town"); //Still need to figure out what cause crazy town... this just minimizes impact
                            headingDelta = headingTargetDir + headingForwardDir;

                        }

                        var pitchDelta = pitchTargetDir - pitchForwardDir;

                        //Debug.Log("TH: " + headingTargetDir + "(" + targetDir.x + ")(" + targetDir.z + ") - FH: " + headingForwardDir + " = Delta: " + headingDelta)
                        //Debug.Log("TH: " + headingTargetDir + ", TP: " + pitchTargetDir + " - FH: " + headingForwardDir + ", FP: " + pitchForwardDir + " - Delta: " + headingDelta + ", " + pitchDelta);
                        GyroAccel.AddFloatToHeadingOffset(-headingDelta * AssistedTargetingCameraSpeed * 3);
                        GyroAccel.AddFloatToPitchOffset(-pitchDelta * AssistedTargetingCameraSpeed * 3);
                    }
                    else
                    {
                        Quaternion newRotation = Quaternion.Slerp(m_PlayerCamera.transform.rotation, Quaternion.LookRotation(head - m_PlayerCamera.transform.position), Time.deltaTime * AssistedTargetingCameraSpeed);
                        Manager.Player.Rotation.Set( new Vector2(newRotation.eulerAngles.x, newRotation.eulerAngles.y) );
                    }


                    // move camera toward target
                    //if(vp_Input.GetAxisRaw("Mouse X") == 0 && vp_Input.GetAxisRaw("Mouse Y") == 0)
                    //{
                    //Quaternion newRotation = Quaternion.Slerp( m_PlayerCamera.transform.rotation, Quaternion.LookRotation(m_CurrentTrackedEnemy.GetComponent<Collider>().bounds.center-m_PlayerCamera.transform.position), Time.deltaTime * AssistedTargetingCameraSpeed);
                    //Quaternion newRotation = Quaternion.Slerp(m_PlayerCamera.transform.rotation, Quaternion.LookRotation(head - m_PlayerCamera.transform.position), Time.deltaTime * AssistedTargetingCameraSpeed);

                    //Manager.Player.Rotation.Set( new Vector2(newRotation.eulerAngles.x, newRotation.eulerAngles.y) );

                    //Debug.Log(newRotation.eulerAngles.x);
                    //GyroAccel.SetHeadingOffset(newRotation.eulerAngles.x);
                    //GyroAccel.SetPitchOffset(newRotation.eulerAngles.y);

                    /*
                    Vector3 targetDir = head - m_PlayerCamera.transform.position;
                    targetDir = targetDir.normalized;
                    Vector3 forward = m_PlayerCamera.transform.forward;
                    forward = forward.normalized;
                    float angleX = Vector3.SignedAngle(new Vector3(forward.x, 0, 0), new Vector3(targetDir.x, 0, 0), Vector3.up);
                    float angleY = Vector3.SignedAngle(new Vector3(0, forward.y, 0), new Vector3(0, targetDir.y, 0), Vector3.up);
                    Debug.Log(angleX + ", " + angleY);
                    GyroAccel.AddFloatToHeadingOffset(angleX);
                    //GyroAccel.AddFloatToPitchOffset(angleY);
                    */

                    /*
                    Quaternion newRotation = Quaternion.Slerp(m_PlayerCamera.transform.rotation, Quaternion.LookRotation(head - m_PlayerCamera.transform.position), Time.deltaTime * AssistedTargetingCameraSpeed);
                    Quaternion rotationDelta = Quaternion.Inverse(m_PlayerCamera.transform.rotation) * newRotation;
                    Debug.Log(rotationDelta.eulerAngles.x + ", " + rotationDelta.eulerAngles.y);
                    float xDeltaAngle = rotationDelta.eulerAngles.x;
                    float yDeltaAngle = rotationDelta.eulerAngles.y;
                    if (xDeltaAngle > 180)
                        xDeltaAngle -= 360;

                    if (yDeltaAngle > 180)
                        yDeltaAngle -= 360;
                    GyroAccel.AddFloatToHeadingOffset(xDeltaAngle);
                    GyroAccel.AddFloatToPitchOffset(yDeltaAngle);
                    */

                    /*Kinda working
                            Vector3 targetDir = head - m_PlayerCamera.transform.position;
                            Vector3 forward = m_PlayerCamera.transform.forward;
                            Vector3 slerpDelta = Vector3.Slerp(targetDir, forward, Time.deltaTime * AssistedTargetingCameraSpeed);

                            float xAngle = Vector3.SignedAngle(forward, slerpDelta, Vector3.up);
                            float yAngle = Vector3.SignedAngle(forward, slerpDelta, Vector3.right);
                            Debug.Log("Offsets: " + GyroAccel.GetHeadingOffset() + ", " + GyroAccel.GetPitchOffset());
                            Debug.Log("Angles: " + xAngle + ", " + yAngle);


                            GyroAccel.AddFloatToHeadingOffset(xAngle/2);
                            //GyroAccel.AddFloatToPitchOffset(-yAngle / 2);
                    */





                    /*
                    Quaternion newRotation = Quaternion.FromToRotation(m_PlayerCamera.transform.forward, head - m_PlayerCamera.transform.position);
                    float newRotationX = newRotation.eulerAngles.x;
                    float newRotationY = newRotation.eulerAngles.y;

                    //if (newRotationX > 180)
                    //    newRotationX -= 360;



                    Debug.Log(newRotationX);
                    GyroAccel.AddFloatToHeadingOffset(newRotationX);
                    //GyroAccel.AddFloatToPitchOffset(newRotationY);
                    */
                    /*
                    Quaternion newRotation = Quaternion.LookRotation(head - m_PlayerCamera.transform.position);

                    Debug.Log(newRotation.eulerAngles.x + " - " + Manager.Player.Rotation.Get().x + " = " + -(newRotation.eulerAngles.x - Manager.Player.Rotation.Get().x));

                    GyroAccel.AddFloatToHeadingOffset(-(Manager.Player.Rotation.Get().x - newRotation.eulerAngles.x));
                    */
                    //GyroAccel.AddFloatToPitchOffset((Manager.Player.Rotation.Get().y - newRotation.eulerAngles.y));

                    //Debug.Log((newRotation.eulerAngles.x - m_PlayerCamera.transform.rotation.eulerAngles.x) + ", " + (newRotation.eulerAngles.y - m_PlayerCamera.transform.rotation.eulerAngles.y));


                    //}
                }
            }
		}
		
		if(!m_Tracking)
			m_CurrentTrackedEnemy = null;
	
	}
	
	
	/// <summary>
	/// Hide crosshair when climbing starts
	/// </summary>
	protected virtual void OnStart_Climb(){ DisplayCrosshair(false); }
	
	
	/// <summary>
	/// Show crosshair when climbing stops
	/// </summary>
	protected virtual void OnStop_Climb(){ DisplayCrosshair(); }
	
	
	/// <summary>
	/// Hide crosshair if zooming
	/// </summary>
	protected virtual void OnStart_Zoom(){ DisplayCrosshair(false); }
	
	
	/// <summary>
	/// Show crosshair when zoom stopped
	/// </summary>
	protected virtual void OnStop_Zoom(){ DisplayCrosshair(); }
	
	protected virtual void OnStart_Run()
	{
	
		if(Manager.Player.CanInteract.Get() || !ScaleOnRun)
			return;
			
		vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", m_CachedScale * ScaleMultiplier, "duration", .15f, "handle", m_ScaleCrosshairHandle) );
		
	}
	
	
	/// <summary>
	/// scale the crosshair to origin when running stops
	/// </summary>
	protected virtual void OnStop_Run()
	{
	
		if(Manager.Player.CanInteract.Get() || !ScaleOnRun)
			return;
	
		vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", m_CachedScale, "duration", .15f, "handle", m_ScaleCrosshairHandle) );
		
	}
	
	
	/// <summary>
	/// Displays the crosshair based on the specified value.
	/// </summary>
	protected virtual void DisplayCrosshair( bool val = true )
	{
	
		m_ShowCrosshair = val;
	
	}
    /*
    IEnumerator AssistedTargetingLOSTimeoutCheck()
    {
        AssistedTargetingLOSTimeoutExpired = false;
        yield return new WaitForSeconds(AssistedTargetingLOSTimeout);
        AssistedTargetingLOSTimeoutExpired = true;
    }
    */

    /// <summary>
    /// Checks if the crosshair is over an enemy
    /// </summary>
    protected void EnemyCheck()
	{
	
		if(Manager.Player.Dead.Active)
			return;
		
		Transform enemy = null;
		RaycastHit hit;
		if(Physics.SphereCast( m_PlayerCamera.transform.position, AssistedTargetingRadius, m_PlayerCamera.transform.forward, out hit, Mathf.Infinity, EnemyMask ))
			//if((EnemyMask.value & 1 << hit.collider.gameObject.layer) != 0)
				if(!m_Enemies.TryGetValue(hit.collider, out enemy))
					m_Enemies.Add(hit.collider, enemy = hit.collider.transform);
				
		if(enemy != null)
        {
            if (AssistedTargetingRequireLineOfSight) {
                Vector3 head = new Vector3(enemy.GetComponent<Collider>().bounds.center.x, enemy.GetComponent<Collider>().bounds.center.y + ((enemy.GetComponent<Collider>().bounds.max.y - enemy.GetComponent<Collider>().bounds.center.y) / 2), enemy.GetComponent<Collider>().bounds.center.z);
                if(Physics.Linecast(m_PlayerCamera.transform.position, head, ~(EnemyMask | 1 << vp_Layer.Trigger)))
                {
                    if (Time.time >= LastTrackedTime + AssistedTargetingLOSTimeout)
                        m_CurrentTrackedEnemy = null;
                    else
                        m_CurrentTrackedEnemy = enemy;
                }
                else
                {

                    Debug.Log("Tracked");
                    m_CurrentTrackedEnemy = enemy;
                    LastTrackedTime = Time.time;
                }
                //m_CurrentTrackedEnemy = Physics.Linecast(m_PlayerCamera.transform.position, head, ~(EnemyMask | 1 << vp_Layer.Trigger)) ? null : enemy;
                var distanceToTarget = (m_PlayerCamera.transform.position - head).magnitude;
                //Debug.Log(distanceToTarget);
                if (distanceToTarget < 3) //lower tracking radius when target in near
                    AssistedTargetingTrackingRadius = 0.5f;
                else
                    AssistedTargetingTrackingRadius = startingAssistedTargetingTrackingRadius;

                //Debug.DrawLine(m_PlayerCamera.transform.position, head, Color.red);
            }
            else
                m_CurrentTrackedEnemy = enemy;
            
        }
            
   
    }
	
	
	/// <summary>
	/// Gets or sets the value of the Crosshair texture
	/// </summary>
	protected virtual Texture OnValue_Crosshair
	{
		get { return m_Renderer.material.mainTexture; }
		set {
			if(m_Tracking && EnemyCrosshair != null)
			{
				m_Renderer.material.mainTexture = EnemyCrosshair;
			}
			else if(value.name == "")
				m_ShowCrosshair = false;
			else
			{
				if(!Manager.Player.Zoom.Active)
					m_ShowCrosshair = true;
				m_Renderer.material.mainTexture = value;
			}
			
			if(m_Tracking)
				return;
		
			// change the icon size if it's not the default 
			Vector3 localScale = value.name == m_CachedTextureName ? m_CachedScale : m_CachedScale * InteractIconSize;
			
			// make the icon size bigger if no icon shown and grabbing
			localScale = Manager.Player.Interactable.Get() != null && Manager.Player.Interactable.Get().GetType() == typeof(vp_Grab) && value.name == "" ? m_CachedScale * (InteractIconSize * 3) : localScale;
			vp_UITween.ScaleTo(m_GameObject, vp_UITween.Hash("scale", localScale, "duration", 0, "handle", m_ScaleCrosshairHandle) );
		}
	}

}

